var gulp = require('gulp');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var clean = require('gulp-clean');

gulp.task('concatjs', function() {
  return gulp.src([
	'node_modules/leaflet-realtime/dist/leaflet-realtime.js',
	'node_modules/beautifymarker/leaflet-beautify-marker-icon.js',
	'node_modules/leaflet-rotatedmarker/leaflet.rotatedMarker.js',
	'node_modules/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.js',
	'node_modules/leaflet-sidebar-v2/js/leaflet-sidebar.js',
	'node_modules/leaflet-canvasicon/leaflet-canvasicon.js',
	'node_modules/leaflet-search/dist/leaflet-search.src.js',
	'lib/leaflet-betterscale/L.Control.BetterScale.js',
	'node_modules/jsonpath-plus/lib/jsonpath.js',
	'node_modules/mustache/mustache.js',
	'node_modules/moment/moment.js',
	'node_modules/@turf/turf/turf.min.js',
	'node_modules/@mapbox/geojsonhint/geojsonhint.js',
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/renderjson/renderjson.js',
	'node_modules/peity/jquery.peity.js',
	'src/js/jquery.sieve.js',
	'src/js/tagsort.min.js',
	'src/js/sortElements.js',
	'src/js/jquery.growl.js',
	'src/js/parse_metar.js',
	'src/L-oscars-dashboard.js',
	'src/dashboard.js',
	'src/L-oscars-util.js',
	'src/L-oscars-zone-group.js',
	'node_modules/beautifymarker/leaflet-beautify-marker.js',
	'src/L-oscars-device-group.js'
    ])
    .pipe(concat('gip-leaflet.js'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('concatcss', function () {
  return gulp.src([
	'node_modules/leaflet/dist/leaflet.css',
	'node_modules/leaflet-pulse-icon/dist/L.Icon.Pulse.css',
	'node_modules/font-awesome/css/font-awesome.css',
	'node_modules/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.css',
	'node_modules/beautifymarker/leaflet-beautify-marker-icon.css',
	'node_modules/leaflet-sidebar-v2/css/leaflet-sidebar.css',
	'lib/leaflet-betterscale/L.Control.BetterScale.css',
	'node_modules/leaflet-search/dist/leaflet-search.src.css',
	'node_modules/leaflet-easybutton/src/easy-button.css',
	'src/css/materialadmin.css',
	'src/css/tagsort.css',
	'src/css/wire.css',
	'src/L-oscars.css'
	])
    .pipe(concatCss("gip-leaflet.css", {rebaseUrls: false}))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('copy', function () {
     return gulp.src([
		'src/images/gip.png',
		'src/images/gip.svg',
		'src/images/gipng-32.png',
		'src/images/gipng-64.png',
		'src/images/gipng-128.png',
		'src/images/gipng.png',
		'src/images/grey-16.png',
		'src/images/leafletjs-16.png',
		'src/images/leafletjs.png',
		'src/images/lgg-logo.svg',
		'src/images/liegeairport-14.png',
		'src/images/liegeairport.png',
		'src/images/loader.gif',
		'src/images/mapbox.png',
		'src/images/osm.png',
		'src/images/powered.png',
		'src/images/red-16.png',
		'src/images/red-32.png',
		'src/images/red.svg',
		'src/images/search-icon.png',
		'src/images/stamen.png'
	])
    .pipe(gulp.dest('./dist/images'));
});

gulp.task('concatjs2', function() {
  return gulp.src([
	'node_modules/leaflet-realtime/dist/leaflet-realtime.js',
	'node_modules/leaflet-pulse-icon/dist/L.Icon.Pulse.js',
	'node_modules/beautifymarker/leaflet-beautify-marker-icon.js',
	'node_modules/beautifymarker/leaflet-beautify-marker.js',
	'node_modules/leaflet-rotatedmarker/leaflet.rotatedMarker.js',
	'node_modules/leaflet-groupedlayercontrol/dist/leaflet.groupedlayercontrol.min.js',
	'node_modules/leaflet-sidebar-v2/js/leaflet-sidebar.js',
	'node_modules/leaflet-canvasicon/leaflet-canvasicon.js',
	'node_modules/leaflet-piechart/leaflet-piechart.js',
	'node_modules/leaflet-search/dist/leaflet-search.src.js',
	'node_modules/leaflet-easybutton/src/easy-button.js',
	'lib/leaflet-betterscale/L.Control.BetterScale.js',
	'node_modules/jsonpath-plus/lib/jsonpath.js',
	'node_modules/mustache/mustache.js',
	'node_modules/moment/moment.js',
	'node_modules/@turf/turf/turf.min.js',
	'node_modules/@mapbox/geojsonhint/geojsonhint.js',
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/renderjson/renderjson.js',
	'node_modules/peity/jquery.peity.js',
	'src/js/jquery.sieve.js',
	'src/js/tagsort.min.js',
	'src/js/sortElements.js',
	'src/js/jquery.growl.js',
	'src/js/parse_metar.js',
	'src/L-oscars-dashboard.js',
	'src/dashboard.js',
	'src/L-oscars-util.js'
    ])
    .pipe(concat('L-oscars-util.js'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('copy2', function () {
     return gulp.src([
		'src/L-oscars-zone-group.js',
		'src/L-oscars-device-group.js',
	])
    .pipe(gulp.dest('./dist/'));
});

gulp.task('clean', function () {
	return gulp.src([
		'./dist/*.{js,css}',
		'./dist/images/*'
	], {read: false})
	.pipe(clean());
});


gulp.task('default', [/*clean,*/'concatjs','concatcss', 'copy', 'concatjs2', 'copy2']);
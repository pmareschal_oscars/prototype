var map, featureList, featureSearch = [], features = [], current_feature, events = [];

function download_features() {
	features = allFeaturesLayer.getGeoJSON();
	var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify({
		type: "FeatureCollection",
		properties : {
			name : "SIMULATOR",
			display_name: "Simulated Devices",
			type: "SIMULATOR",
			display_type: "Simulated Devices",
			status: "ACTIVE",
			layer_group_display_name: "Simulated Devices"
		},
		features: features
	}));
	var dlAnchorElem = document.getElementById('downloadAnchorElem');
	dlAnchorElem.setAttribute("href",     dataStr     );
	dlAnchorElem.setAttribute("download", "featurecollection.json");
	dlAnchorElem.click();
	return false;
}

function download_gipevents() {
	var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(events));
	var dlAnchorElem = document.getElementById('downloadAnchorElem');
	dlAnchorElem.setAttribute("href",     dataStr     );
	dlAnchorElem.setAttribute("download", "featurecollection.json");
	dlAnchorElem.click();
	return false;
}

$(window).resize(function() {
  sizeLayerControl();
});

$(document).on("click", ".feature-row td:nth-child(2)", function(e) {
  $(document).off("mouseout", ".feature-row", clearHighlight);
  sidebarClick(parseInt($(this).parent().attr("id"), 10));
});

$(document).on("click", "button[role='feature-action']", function(e) {
	// hierarchy: table tbody tr td btn-grp button
  var layerId = parseInt($(this).parent().parent().parent().attr("id"), 10);
  var action = $(this).data('action');
  switch(action) {
	  case "properties":
	    sidebarClick(layerId);
	    break;
	  case "start":
	    break;
	  case "pause":
	    break;
	  case "duplicate":
	    break;
	  case "remove":
	    break;
  }
});

if ( !("ontouchstart" in window) ) {
  $(document).on("mouseover", ".feature-row", function(e) {
    highlight.clearLayers().addLayer(L.circleMarker([$(this).attr("lat"), $(this).attr("lng")], highlightStyle));
  });
}

$(document).on("mouseout", ".feature-row", clearHighlight);

$("#about-btn").click(function() {
  $("#aboutModal").modal("show");
  $(".navbar-collapse.in").collapse("hide");
  return false;
});

$("#list-btn").click(function() {
  animateSidebar();
  return false;
});

$("#nav-btn").click(function() {
  $(".navbar-collapse").collapse("toggle");
  return false;
});

$("#sidebar-toggle-btn").click(function() {
  animateSidebar();
  return false;
});

$("#sidebar-hide-btn").click(function() {
  animateSidebar();
  return false;
});

function animateSidebar() {
  $("#sidebar").animate({
    width: "toggle"
  }, 350, function() {
    map.invalidateSize();
  });
}

function sizeLayerControl() {
  $(".leaflet-control-layers").css("max-height", $("#map").height() - 50);
}

function clearHighlight() {
  highlight.clearLayers();
}

function sidebarClick(id) {
  var layer = allFeaturesLayer.getLayer(id);
  map.setView([layer.getLatLng().lat, layer.getLatLng().lng], 17);
  layer.fire("click");
  /* Hide sidebar and go to the map on small screens */
  if (document.body.clientWidth <= 767) {
    $("#sidebar").hide();
    map.invalidateSize();
  }
}

function syncSidebar() {
  var actions = '<div class="btn-group" role="group">'
		+'<button type="button" class="btn btn-success btn-xs" role="feature-action" data-action="properties">P</button>'
		+'<button type="button" class="btn btn-warning btn-xs" role="feature-action" data-action="start"     ><i class="glyphicon glyphicon-step-backward"></i></button>'
		+'<button type="button" class="btn btn-success btn-xs" role="feature-action" data-action="pause"     ><i class="glyphicon glyphicon-play"></i></button>'
		+'<button type="button" class="btn btn-primary btn-xs" role="feature-action" data-action="duplicate" ><i class="glyphicon glyphicon-duplicate"></i></button>'
		+'<button type="button" class="btn btn-danger  btn-xs" role="feature-action" data-action="remove"    ><i class="glyphicon glyphicon-remove"></i></button>'
		+'</div>';
  /* Empty sidebar features */
  $("#feature-list tbody").empty();
  /* Loop through allFeaturesLayer layer and add only features which are in the map bounds */
  allFeaturesLayer.eachLayer(function (layer) {
    if (map.hasLayer(featureLayer)) {
      if (map.getBounds().contains(layer.getLatLng())) {
        $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng 
		+ '"><td style="vertical-align:middle;">'+ actions +'</td><td class="feature-name">' 
		+ layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      }
    }
  });
  /* Update list.js featureList */
  featureList = new List("features", {
    valueNames: ["feature-name"]
  });
  featureList.sort("feature-name", {
    order: "asc"
  });
}

/* Overlay Layers */
var highlight = L.geoJson(null);
var highlightStyle = {
  stroke: false,
  fillColor: "#00FFFF",
  fillOpacity: 0.7,
  radius: 10
};

var editor = new JSONEditor(document.getElementById("json-editor"),{
	theme: 'bootstrap3',
  schema: {
	  "title": "Properties",
	  "type": "object",
	  "properties": {
	    "name": {
	      "type": "string",
	      "description": "Internal identification name",
	      "minLength": 2
	    },
	    "display_name": {
	      "type": "string",
	      "description": "Displayed unique name",
	      "minLength": 4
	    },
	    "status": {
	      "type": "string",
	      "description": "Device status. Determines device displayed color.",
	      "enum": [
	        "info",
	        "success",
	        "danger",
	        "warning",
	        "default",
	        "primary",
	        "accent"
	      ]
		  },
		  "type": {
		    "type": "string",
	      "description": "Device status. Determines device icon.",
		    "enum": [
		      "plane",
					"paper-plane",
					"fighter-jet",
		      "car",
		      "truck",
		      "bus",
		      "motorcycle",
		      "ambulance",
		      "taxi",
					"bicycle",
					"map",
					"map-marker",
					"location-arrow"				
		    ]
		  },
		"sampling": {
	      "type": "integer",
	      "description": "Device sampling interval (in seconds, maximum 10 minutes).",
	      "default": 30,
	      "minimum": 1,
	      "maximum": 600
	    },
	    "segment_speeds": {
	      "type": "array",
	      "format": "table",
	      "title": "Segment Speed",
	      "items": {
		      "type": "integer",
		      "description": "Device speed in km/h (0-1000 km/h).",
		      "default": 200,
		      "minimum": 0,
		      "maximum": 1000
		    }
	    }
	  }
	},
	//
	disable_edit_json: true,
	disable_properties: true,
	disable_collapse: true,
	no_additional_properties: true,
	required_by_default: true
});

/* Single marker layer to hold all layers */
var containerLayer = new L.LayerGroup();

/* Empty layer placeholder to add to layer control for listening when to add/remove allFeaturesLayer to containerLayer layer */
var featureLayer = L.geoJson(null);
var allFeaturesLayer = L.geoJson(null, {
  pointToLayer: function (feature, latlng) {
    return L.marker(latlng, {
      title: feature.properties.NAME,
      riseOnHover: true
    });
  },
  onEachFeature: function (feature, layer) {
    if (feature.properties) {
      //var content = "<table class='table table-striped table-bordered table-condensed'>" + "<tr><th>Name</th><td>" + feature.properties.NAME + "</td></tr>" + "<tr><th>Phone</th><td>" + feature.properties.TEL + "</td></tr>" + "<tr><th>Address</th><td>" + feature.properties.ADDRESS1 + "</td></tr>" + "<tr><th>Website</th><td><a class='url-break' href='" + feature.properties.URL + "' target='_blank'>" + feature.properties.URL + "</a></td></tr>" + "<table>";
      layer.on({
        click: function (e) {
					editor.setValue({
						name: feature.properties.NAME,
						display_name: feature.properties.NAME			
					});
          $("#feature-title").html(feature.properties.NAME);
          $("#featureModal").modal("show");
          highlight.clearLayers().addLayer(L.circleMarker([feature.geometry.coordinates[1], feature.geometry.coordinates[0]], highlightStyle));
        }
      });
      $("#feature-list tbody").append('<tr class="feature-row" id="' + L.stamp(layer) + '" lat="' + layer.getLatLng().lat + '" lng="' + layer.getLatLng().lng + '"><td style="vertical-align: middle;"></td><td class="feature-name">' + layer.feature.properties.NAME + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
      featureSearch.push({
        name: layer.feature.properties.NAME,
        address: layer.feature.properties.ADDRESS1,
        source: "Theaters",
        id: L.stamp(layer),
        lat: layer.feature.geometry.coordinates[1],
        lng: layer.feature.geometry.coordinates[0]
      });
    }
  }
});
$.getJSON("data/sample.geojson", function (data) {
  allFeaturesLayer.addData(data);
  map.addLayer(featureLayer);
});

/* Basemap Layers */
var osm = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {attribution: '<a href="http://openstreetmap.org">OpenStreetMap</a>'});

map = L.map("map", {
  zoom: 10,
  center: [40.702222, -73.979378],
  layers: [osm, containerLayer, highlight],
  zoomControl: false,
  attributionControl: false
});

/* Layer control listeners that allow for a single containerLayer layer */
map.on("overlayadd", function(e) {
  if (e.layer === featureLayer) {
    containerLayer.addLayer(allFeaturesLayer);
    syncSidebar();
  }
});

map.on("overlayremove", function(e) {
  if (e.layer === featureLayer) {
    containerLayer.removeLayer(allFeaturesLayer);
    syncSidebar();
  }
});

/* Filter sidebar feature list to only show features in current map bounds */
map.on("moveend", function (e) {
  syncSidebar();
});

/* Clear feature highlight when map is clicked */
map.on("click", function(e) {
  highlight.clearLayers();
});

var zoomControl = L.control.zoom({
  position: "bottomright"
}).addTo(map);


/* Larger screens get expanded layer control and visible sidebar */
if (document.body.clientWidth <= 767) {
  var isCollapsed = true;
} else {
  var isCollapsed = false;
}

var baseLayers = {
  "Street Map": osm
};

var groupedOverlays = {
  "Devices": {
    "Simulated": featureLayer
  }
};

var layerControl = L.control.groupedLayers(baseLayers, groupedOverlays, {
  collapsed: isCollapsed
}).addTo(map);

/* Highlight search box text on click */
$("#searchbox").click(function () {
  $(this).select();
});

/* Prevent hitting enter from refreshing the page */
$("#searchbox").keypress(function (e) {
  if (e.which == 13) {
    e.preventDefault();
  }
});

$("#featureModal").on("hidden.bs.modal", function (e) {
  $(document).on("mouseout", ".feature-row", clearHighlight);
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function () {
  $("#loading").hide();
  sizeLayerControl();
  /* Fit map to boroughs bounds */
  featureList = new List("features", {valueNames: ["feature-name"]});
  featureList.sort("feature-name", {order:"asc"});

  var allFeaturesLayerBH = new Bloodhound({
    name: "Theaters",
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: featureSearch,
    limit: 10
  });

  allFeaturesLayerBH.initialize();

  /* instantiate the typeahead UI */
  $("#searchbox").typeahead({
    minLength: 3,
    highlight: true,
    hint: false
  }, {
    name: "Theaters",
    displayKey: "name",
    source: allFeaturesLayerBH.ttAdapter(),
    templates: {
      header: "<h4 class='typeahead-header'>Theaters</h4>",
      suggestion: Handlebars.compile(["{{name}}<br>&nbsp;<small>{{address}}</small>"].join(""))
    }
  }).on("typeahead:selected", function (obj, datum) {
    if (datum.source === "Theaters") {
      if (!map.hasLayer(featureLayer)) {
        map.addLayer(featureLayer);
      }
      map.setView([datum.lat, datum.lng], 17);
      if (map._layers[datum.id]) {
        map._layers[datum.id].fire("click");
      }
    }
    if ($(".navbar-collapse").height() > 50) {
      $(".navbar-collapse").collapse("hide");
    }
  }).on("typeahead:opened", function () {
    $(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
    $(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
  }).on("typeahead:closed", function () {
    $(".navbar-collapse.in").css("max-height", "");
    $(".navbar-collapse.in").css("height", "");
  });
  $(".twitter-typeahead").css("position", "static");
  $(".twitter-typeahead").css("display", "block");
});

// Leaflet patch to make layer control scrollable on touch browsers
var container = $(".leaflet-control-layers")[0];
if (!L.Browser.touch) {
  L.DomEvent
  .disableClickPropagation(container)
  .disableScrollPropagation(container);
} else {
  L.DomEvent.disableClickPropagation(container);
}

//
var drawnItems = L.featureGroup().addTo(map);
map.addControl(new L.Control.Draw({
    edit: {
        featureGroup: drawnItems,
        poly: {
            allowIntersection: false
        }
    },
    draw: {
			polygon: false,
			rectangle: false,
			circle: false,
        polyline: true
    }
}));

map.on(L.Draw.Event.CREATED, function (event) {
    var layer = event.layer;

    drawnItems.addLayer(layer);
});

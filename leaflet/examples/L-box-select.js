/*
 * LeafletJS Oscars GIP Map Widget Helper
 * 2017 Pierre M
 * License: MIT
 */
"use strict";

L.Oscars = L.Oscars || {};

L.Oscars.BoxSelect = L.Control.BoxZoom.extend({

    handleMouseDown: function (event) {
		var old = this.map.boxZoom._onMouseUp;
		this.map.boxZoom._onMouseUp = function (e) {
			if ((e.which !== 1) && (e.button !== 1)) { return; }

			this._finish();

			if (!this._moved) { return; }
			// Postpone to next JS tick so internal click event handling
			// still see it as "moved".
			setTimeout(L.bind(this._resetState, this), 0);

			var bounds = new L.LatLngBounds(
			        this._map.containerPointToLatLng(this._startPoint),
			        this._map.containerPointToLatLng(this._point));

			// this.map.on('boxselectend', this.setStateOff, this);
			this._map
			//	.fitBounds(bounds) // !!
				.fire('boxzoomend', {boxZoomBounds: bounds});
		}
        this.map.boxZoom._onMouseDown.call(this.map.boxZoom, { clientX:event.originalEvent.clientX, clientY:event.originalEvent.clientY, which:1, shiftKey:true });
		this.map.boxZoom._onMouseUp = old;
    }
	
});

L.Oscars.boxselect = function(src, options) {
    return new L.Oscars.BoxSelect(src, options);
};
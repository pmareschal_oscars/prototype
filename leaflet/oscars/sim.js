// http://stackoverflow.com/questions/37166172/mapbox-tiles-and-leafletjs
//streets-v9
//satellite-streets-v9
//light-v9
//dark-v9
//outdoors-v9
var mapboxType = 'dark';
var mapboxName = "Mapbox "+mapboxType;
var mapboxUrl = 'https://api.mapbox.com/styles/v1/mapbox/'+mapboxType+'-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGV2bGVha3MiLCJhIjoiY2oxcDgyOTZvMDA0NDMyb3R0aW51c3BpZiJ9.4UBU6GK4ziylPRubY-n-KQ',
    mapboxLink = '<a href="http://www.mapbox.com/">Mapbox</a>';

var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
    thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    osmAttrib = '&copy; ' + osmLink + ' Contributors',
    landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
    thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
    landMap = L.tileLayer(landUrl, {attribution: thunAttrib}),
    mapboxMap = L.tileLayer(mapboxUrl, {attribution: mapboxLink});

var Stamen_Watercolor = L.tileLayer('http://tile.stamen.com/watercolor/{z}/{x}/{y}.png', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	subdomains: 'abcd',
	minZoom: 1,
	maxZoom: 16,
	ext: 'png'
});
	
var baseLayers = {
	"OSM Mapnik": osmMap,
	"Stamen Watercolor": Stamen_Watercolor,
	"Landscape": landMap
};
baseLayers[mapboxName] = mapboxMap;

var airportOverlay = new L.ImageOverlay("data/EBLG_GMC01_v13.svg", new L.LatLngBounds(
	new L.LatLng(50.62250,5.41630),		// en bas à gauche
	new L.LatLng(50.65655,5.47567)), {	// en haut à droite 
	opacity: 0.8
});

// Create map
var map = L.Oscars.Util.map({
	id: "map",
	center: [50.639, 5.450],
	zoom: 15,
	width: 800,
	height: 600,
	name: "STATICTESTMAP",
	display_name: "A Static Test Map for GIP",
	layers: [osmMap],
	layerControl: {
		baseLayers: baseLayers,
		overlays: {
			"<span style='color: #0C64AF;'><img src='oscars/i/liegeairport-14.png'>&nbsp;Liège Airport</span>": {
				"<span style='color: #EE850A;'>Ground Map</span>": airportOverlay
			}
		},
		options: { groupCheckboxes: true , collapsed: false}
	},
	sidebar: true,
	search: true,
	reset: true,
	betterScale: true,
	stylesets: {
		DEFAULT: "Normal",
		ALT: "Inverted"
	}
});

airportOverlay.addTo(map);

// Add zone groups (decoration)
var tracks = L.Oscars.zoneGroup(egblLandingTracks);

var parkings = L.Oscars.zoneGroup(eblgParkings).addTo(map);

var parkings4snow = L.Oscars.zoneGroup(eblgParkings4snow).addTo(map);

(beaconZones = function() {
	eblgEmpty.properties.display_name = "Beacons";
	var beaconLayer = L.Oscars.zoneGroup(eblgEmpty);
	
	function getFeature(name) {
		var found = null;
		beacons.features.forEach(function(feature) {
			if(feature.properties.name == name)
				found = feature;
		});
		return found;
	}

	function getLatLng(feature) {
		return L.latLng(feature.geometry.coordinates[1],feature.geometry.coordinates[0]);
	}

	var biw = 0.1,
	    bmw = 0.2,
		bow = 0.5;
	var trackName = "5L";
	var t1 = getFeature("NO");
	var t2 = getFeature("NE");
	var im = getFeature(trackName + "I");
	var tb = turf.bearing(t1, t2);

	// the track's end that is the further away is the "end" of the track
	var d1 = turf.distance(t1, im);
	var d2 = turf.distance(t2, im);
	var te = d1 > d2 ? t1 : t2;

	b1 = turf.destination(im, biw, tb + 90);
	b2 = turf.destination(im, biw, tb - 90);
	b3 = turf.destination(te, biw, tb + 90);
	b4 = turf.destination(te, biw, tb - 90);
	L.circleMarker(getLatLng(im), {radius: 10, fillColor: "white", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "white", fillOpacity: 0.2, weight: 1, color: "white", dashArray: "4 8"}
		}
	});

	var mm = getFeature(trackName + "M");
	b1 = turf.destination(mm, bmw, tb + 90);
	b2 = turf.destination(mm, bmw, tb - 90);
	L.circleMarker(getLatLng(mm), {radius: 10, fillColor: "orange", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "orange", fillOpacity: 0.2, weight: 1, color: "orange", dashArray: "16 8 4 8"}
		}
	});

	var om = getFeature(trackName + "O");
	b1 = turf.destination(om, bow, tb + 90);
	b2 = turf.destination(om, bow, tb - 90);
	L.circleMarker(getLatLng(om), {radius: 10, fillColor: "blue", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "blue", fillOpacity: 0.2, weight: 1, color: "blue", dashArray: "16 8"}
		}
	});

})();

/**
 *	Add a layer for Geolocation Simulator devices
 */
var simulator = L.Oscars.deviceGroup({
	"type": "FeatureCollection",
	"properties" : {
		"name" : "SIMULATOR",
		"display_name": "Simulated Vehicles",
		"type" : "*",
		"display_type": "All",
		"status": "ACTIVE",
		"layer_group_display_name": "Devices"
	},
	"features": []
}).addTo(map);

window.addEventListener("storage", function (ev) {
    if (ev.key == 'GIPSIMPOS') {
        var device=JSON.parse(ev.newValue);
		console.log(device);
		simulator.update(device);
    }
});

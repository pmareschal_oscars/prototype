var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
    osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    osmAttrib = '&copy; ' + osmLink + ' Contributors';

var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib});

var map = L.map("map", {
	center: [50.639, 5.450],
	zoom: 15,
	layers: [osmMap],
	attributionControl: false
});
var eblg = L.geoJSON(eblgBig).addTo(map);
// http://stackoverflow.com/questions/37166172/mapbox-tiles-and-leafletjs
//streets-v9
//satellite-streets-v9
//light-v9
//dark-v9
//outdoors-v9
var mapboxType = 'dark';
var mapboxName = "Mapbox "+mapboxType;
var mapboxUrl = 'https://api.mapbox.com/styles/v1/mapbox/'+mapboxType+'-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGV2bGVha3MiLCJhIjoiY2oxcDgyOTZvMDA0NDMyb3R0aW51c3BpZiJ9.4UBU6GK4ziylPRubY-n-KQ',
    mapboxLink = '<a href="http://www.mapbox.com/">Mapbox</a>';

var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
    thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    osmAttrib = '&copy; ' + osmLink + ' Contributors',
    landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
    thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
    landMap = L.tileLayer(landUrl, {attribution: thunAttrib}),
    mapboxMap = L.tileLayer(mapboxUrl, {attribution: mapboxLink});

var Stamen_Watercolor = L.tileLayer('http://tile.stamen.com/watercolor/{z}/{x}/{y}.png', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	subdomains: 'abcd',
	minZoom: 1,
	maxZoom: 16,
	ext: 'png'
});
	
var baseLayers = {
	"OSM Mapnik": osmMap,
	"Stamen Watercolor": Stamen_Watercolor,
	"Landscape": landMap
};
baseLayers[mapboxName] = mapboxMap;

var airportOverlay = new L.ImageOverlay("data/EBLG_GMC01_v13.svg", new L.LatLngBounds(
	new L.LatLng(50.62250,5.41630),		// en bas à gauche
	new L.LatLng(50.65655,5.47567)), {	// en haut à droite 
	opacity: 1
});

// Create map
var map = L.map("map", {
	center: [50.639, 5.450],
	zoom: 15,
	layers: [osmMap],
	attributionControl: false
});
L.Oscars.Util.prepareMap(map, {
	name: "STATICTESTMAP",
	display_name: "A Static Test Map for GIP",
	layerControl: {
		baseLayers: baseLayers,
		overlays: {
			"<span style='color: #0C64AF;'><img src='oscars/i/liegeairport-14.png'>&nbsp;Liège Airport</span>": {
				"<span style='color: #EE850A;'>Ground Map</span>": airportOverlay
			}
		},
		options: { groupCheckboxes: true , collapsed: false }
	},
	betterScale: true,
	track: true,
	speedVector: true,
	info: true
});
/*
var map = L.Oscars.Util.map({
	id: "map",
	center: [50.639, 5.450],
	zoom: 15,
	width: 800,
	height: 600,
	name: "STATICTESTMAP",
	display_name: "A Static Test Map for GIP",
	layers: [osmMap],
	layerControl: {
		baseLayers: baseLayers,
		overlays: {
			"<span style='color: #0C64AF;'><img src='oscars/i/liegeairport-14.png'>&nbsp;Liège Airport</span>": {
				"<span style='color: #EE850A;'>Ground Map</span>": airportOverlay
			}
		},
		options: { groupCheckboxes: true , collapsed: false}
	},
	betterScale: true,
	track: true,
	speedVector: true,
	reset: true,
	search: true,
	info: true,
	wire: true,
	voice: false,
	stylesets: {
		DEFAULT: "Normal",
		ALT: "Inverted"
	},
	about: true
});
*/

var sensors = L.Oscars.deviceGroup(eblgSensor).addTo(map);

var autoUpd = eblgSensor.features[4];

jQuery(document).ready(function($){
	setInterval(function() {
		autoUpd.properties._data.values.shift();
		autoUpd.properties._data.values.push(Math.round(Math.random() * 10));
		sensors.update(autoUpd);
	}, 1000)
});


[egblLandingTracks,eblgParkings,eblgParkings4snow,eblgPol].forEach(function(fc) {
	//spitsdo(fc);
});

[eblgAircrafts,eblgSnow,eblgGround,eblgAircrafts1,eblgAircrafts2,eblgGround1,eblgGround2,eblgSnow1,eblgParkings1,eblgParkings4snow1].forEach(function(fc) {
	//spitevt(fc);
});

jQuery(document).ready(function($){$('#gip-version').html(L.Oscars.version);});

// http://stackoverflow.com/questions/37166172/mapbox-tiles-and-leafletjs
//streets-v9
//satellite-streets-v9
//light-v9
//dark-v9
//outdoors-v9
var mapboxType = 'dark';
var mapboxName = "Mapbox "+mapboxType;
var mapboxUrl = 'https://api.mapbox.com/styles/v1/mapbox/'+mapboxType+'-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGV2bGVha3MiLCJhIjoiY2oxcDgyOTZvMDA0NDMyb3R0aW51c3BpZiJ9.4UBU6GK4ziylPRubY-n-KQ',
    mapboxLink = '<a href="http://www.mapbox.com/">Mapbox</a>';

var osmLink = '<a href="http://openstreetmap.org">OpenStreetMap</a>',
    thunLink = '<a href="http://thunderforest.com/">Thunderforest</a>';

var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    osmAttrib = '&copy; ' + osmLink + ' Contributors',
    landUrl = 'http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png',
    thunAttrib = '&copy; '+osmLink+' Contributors & '+thunLink;

var osmMap = L.tileLayer(osmUrl, {attribution: osmAttrib}),
    landMap = L.tileLayer(landUrl, {attribution: thunAttrib}),
    mapboxMap = L.tileLayer(mapboxUrl, {attribution: mapboxLink});

var Stamen_Watercolor = L.tileLayer('http://tile.stamen.com/watercolor/{z}/{x}/{y}.png', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	subdomains: 'abcd',
	minZoom: 1,
	maxZoom: 16,
	ext: 'png'
});
	
var baseLayers = {
	"OSM Mapnik": osmMap,
	"Stamen Watercolor": Stamen_Watercolor,
	"Landscape": landMap
};
baseLayers[mapboxName] = mapboxMap;

var airportOverlay = new L.ImageOverlay("data/EBLG_GMC01_v13.svg", new L.LatLngBounds(
	new L.LatLng(50.62250,5.41630),		// en bas à gauche
	new L.LatLng(50.65655,5.47567)), {	// en haut à droite 
	opacity: 1
});

var airportNightOverlay = new L.ImageOverlay("data/EBLG_GMC01_v13-night.svg", new L.LatLngBounds(
	new L.LatLng(50.62250,5.41630),		// en bas à gauche
	new L.LatLng(50.65655,5.47567)), {	// en haut à droite 
	opacity: 1
});

// Create map
/*
var map = L.Oscars.Util.map({
	id: "map",
	center: [50.639, 5.450],
	zoom: 15,
	width: 800,
	height: 600,
	name: "STATICTESTMAP",
	display_name: "A Static Test Map for GIP",
	layers: [osmMap],
	layerControl: {
		baseLayers: baseLayers,
		overlays: {
			"<span style='color: #0C64AF;'><img src='oscars/i/liegeairport-14.png'>&nbsp;Liège Airport</span>": {
				"<span style='color: #EE850A;'>Ground Map</span>": airportOverlay
			}
		},
		options: { groupCheckboxes: true , collapsed: false}
	},
	sidebar: true,
	search: true,
	reset: true,
	betterScale: true,
	voice: false,
	track: true
});
*/
// Create map
var map = L.map("map", {
	center: [50.64, 5.445],
	zoom: 15,
	layers: [osmMap],
	attributionControl: false
});

//map.prepareMap(/*INTENTIONALY EMPTY*/);
console.log("no prepare map, L.Oscars release ", L.Oscars.version);

L.geoJSON(eblgTaxi, {
    style: function (feature) {
        return {color: "yellow", opacity: 0.2};
    }
}).addTo(map);

// Add zone groups (decoration)
var tracks = L.Oscars.zoneGroup(egblLandingTracks);

var parkings = L.Oscars.zoneGroup(eblgParkings).addTo(map);

var parkings4snow = L.Oscars.zoneGroup(eblgParkings4snow).addTo(map);

// Add device groups (decoration)
var snow = L.Oscars.deviceGroup({
   "type":"FeatureCollection",
	"properties" : {
		"name" : "SNOWVEHICLE",
		"display_name": "Snow Vehicles",
		"type" : "SNOW",
		"display_type": "Snow Vehicles",
		"status": "ACTIVE",
		"layer_group_display_name": "Ground Vehicles"
	},
   "features":[]}, {
	zoom_min: 13,
	zoom_max: 18
}).addTo(map);

var aircrafts = L.Oscars.deviceGroup(eblgAircrafts, {
	greyOut: 15000,
	takeOut: 22000,
	search: true,
	gipDefaults: {
		SPEED_VECTOR: {
			length: 8
		},
		TRACK: {
			color: 'green'
		}
	}	
}).addTo(map);


(beaconZones = function() {
	eblgEmpty.properties.display_name = "Beacons";
	var beaconLayer = L.Oscars.zoneGroup(eblgEmpty);//.addTo(map);
	
	function getFeature(name) {
		var found = null;
		beacons.features.forEach(function(feature) {
			if(feature.properties.name == name)
				found = feature;
		});
		return found;
	}

	function getLatLng(feature) {
		return L.latLng(feature.geometry.coordinates[1],feature.geometry.coordinates[0]);
	}

	var biw = 0.1,
	    bmw = 0.2,
		bow = 0.5;
	var trackName = "5L";
	var t1 = getFeature("NO");
	var t2 = getFeature("NE");
	var im = getFeature(trackName + "I");
	var tb = turf.bearing(t1, t2);

	// the track's end that is the further away is the "end" of the track
	var d1 = turf.distance(t1, im);
	var d2 = turf.distance(t2, im);
	var te = d1 > d2 ? t1 : t2;

	b1 = turf.destination(im, biw, tb + 90);
	b2 = turf.destination(im, biw, tb - 90);
	b3 = turf.destination(te, biw, tb + 90);
	b4 = turf.destination(te, biw, tb - 90);
	L.circleMarker(getLatLng(im), {radius: 10, fillColor: "white", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "white", fillOpacity: 0.2, weight: 1, color: "white", dashArray: "4 8"}
		}
	});

	var mm = getFeature(trackName + "M");
	b1 = turf.destination(mm, bmw, tb + 90);
	b2 = turf.destination(mm, bmw, tb - 90);
	L.circleMarker(getLatLng(mm), {radius: 10, fillColor: "orange", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "orange", fillOpacity: 0.2, weight: 1, color: "orange", dashArray: "16 8 4 8"}
		}
	});

	var om = getFeature(trackName + "O");
	b1 = turf.destination(om, bow, tb + 90);
	b2 = turf.destination(om, bow, tb - 90);
	L.circleMarker(getLatLng(om), {radius: 10, fillColor: "blue", fillOpacity: 1, weight: 0}).addTo(beaconLayer);
	beaconLayer.addData({
		type: "Feature",
		geometry: {
			type: "Polygon",
			coordinates: [[b1.geometry.coordinates, b2.geometry.coordinates, b4.geometry.coordinates, b3.geometry.coordinates, b1.geometry.coordinates]]
		},
		properties: {
			_style: {fillColor: "blue", fillOpacity: 0.2, weight: 1, color: "blue", dashArray: "16 8"}
		}
	});

})();

/**
 *	Add a layer for Geolocation Simulator devices
 */
var simulator = L.Oscars.deviceGroup({
	"type": "FeatureCollection",
	"properties" : {
		"name" : "SIMULATOR",
		"display_name": "Simulated Vehicles",
		"type" : "*",
		"display_type": "All",
		"status": "ACTIVE",
		"layer_group_display_name": "Devices"
	},
	"features": eblgSnow.features
}).addTo(map);

window.addEventListener("storage", function (ev) {
    if (ev.key == 'GIPSIMPOS') {
        var device=JSON.parse(ev.newValue);
		console.log(device);
		simulator.update(device);
    }
});

/**
 *	Simulate GIP GeoJSON Feature Update
*/

//setInterval(L.bind(aircrafts.cleanUp, aircrafts), 5000);

var ground;

setTimeout(function () {
	// Plane turns on taxiway
	// Simulate device movement and appareance change
	console.log('TEST', 'updated device 1');
	aircrafts.update(eblgAircrafts1);
	snow.update(eblgSnow1);

	ground = L.Oscars.deviceGroup(eblgGround).addTo(map);

}, 3000);

setTimeout(function () {
	console.log('TEST', 'updated device 2');
	aircrafts.update(eblgAircrafts2);
	ground.update(eblgGround1);
	console.log('TEST', 'updated zones');
	eblgParkings.features[1].properties.display_status = 'BUSY';
	eblgParkings.features[1].properties._style = {
	    color: "rgb(255,0,0)",
	    weight: 1,
	    opacity: 0.8,
		fillColor: "rgb(255,0,0)",
	    fillOpacity: 0.4
	};
	parkings.update(eblgParkings.features[1]);

	var pol = L.Oscars.zoneGroup(eblgPol, {
		gipDefaults: {
			STYLE: {
				color:  "green"
			}
		}
	}).addTo(map);
}, 6000);

setTimeout(function () {
	console.log('TEST', 'updated device 3');
	ground.update(eblgGround2);
}, 9000);



var sensors = L.Oscars.deviceGroup(eblgSensor, {
	zoom_min: 13,
	zoom_max: 18
}).addTo(map);

/*
 * Simulate live update of graph
 */
var autoUpd = eblgSensor.features[4];

jQuery(document).ready(function($){
	setInterval(function() {
		autoUpd.properties._data.values.shift();
		autoUpd.properties._data.values.push(Math.round(Math.random() * 10));
		sensors.update(autoUpd);
	}, 1000)
});

/*
2001,  -- point
2003,  -- 2-dimensional polygon
8307, -- WSG84
*/
var group_name = null;
function spitevt(geojson) {
	if(geojson.type == "FeatureCollection") {
		if(geojson.properties && geojson.properties.name)
			group_name = geojson.properties.name;
		geojson.features.forEach(function(feature){
			spitevt(feature);
		});
	} else if (Array.isArray(geojson)) {
		geojson.forEach(function(feature){
			spitevt(feature);
		});
	} else {
		var pr = geojson.properties, str = null;
		if(group_name) {
			geojson.properties = geojson.properties || {};
			geojson.properties.group_name = group_name;
		}
		var geojson_clean = JSON.parse(JSON.stringify(geojson))
		delete geojson_clean._style;
		delete geojson_clean._styles;
		delete geojson_clean._template;
		delete geojson_clean._templates;
		
		console.log( JSON.stringify( {
			GROUP_NAME: group_name,
			OBJ_NAME: pr.name,
			OBJ_TYPE: pr.type,
			OBJ_STATUS: pr.status,
			OBJ_DISPLAY_STATUS: pr.display_status,
			OBJ_EVENT: JSON.stringify(geojson_clean),
			OBJ_EVENT_STYLED: JSON.stringify(geojson)
		} ) );
	}
}

function spitsdo(geojson) {
	var mkarr = function(coords) {
		var c = [];
		coords.forEach(function(val) {
			c.push(val[0]);
			c.push(val[1]);
		})
		return c.join(',');
	}

	if(geojson.type == "FeatureCollection") {
		if(geojson.properties && geojson.properties.name)
			group_name = geojson.properties.name;
		geojson.features.forEach(function(feature){
			spitsdo(feature);
		});
	} else if (Array.isArray(geojson)) {
		geojson.forEach(function(feature){
			spitsdo(feature);
		});
	} else {
		var pr = geojson.properties;
		var sdo = null;
		if(geojson.geometry.type == "Point") {
			sdo = 'MDSYS.SDO_GEOMETRY(2001,4326,MDSYS.SDO_POINT_TYPE('+geojson.geometry.coordinates[0]+','+geojson.geometry.coordinates[1]+',null)'+',null,null)';
		} else {
			var arr = mkarr(geojson.geometry.coordinates[0]);
			sdo = 'MDSYS.SDO_GEOMETRY(2003,4326,null,MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY('+arr+'))';
		}
		//var sdo_type = (geojson.type == "Point") ? '2001' : '2003';
		//var sdo_crs  = '8307';
		//var sdo = 'MDSYS.SDO_GEOMETRY('+sdo_type+','+sdo_crs+','+'test'+')'
		console.log( JSON.stringify( {
			NAME: pr.name,
			DISPLAY_NAME: pr.display_name,
			ZONE_TYPE: pr.type,
			STATUS: pr.status,
			DISPLAY_STATUS: pr.display_status
			//GEOM2D: sdo
			//DESCRIPTION: JSON.stringify(geojson.geometry)
		} ) );
		console.log( JSON.stringify( {
			SERVICE_NAME: 'CREATE_ZONE',
			GROUP_NAME: group_name,
			OBJ_NAME: pr.name,
			OBJ_TYPE: pr.type,
			OBJ_STATUS: pr.status,
			OBJ_DISPLAY_STATUS: pr.display_status,
			OBJ_EVENT: JSON.stringify(geojson.geometry),
		} ) );
	}
}
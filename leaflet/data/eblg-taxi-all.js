var eblgTaxi = {
	"type": "FeatureCollection",
	"features": [
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "LT_NORD"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.453617572784424,
						50.64609979931754
					],
					[
						5.430207252502441,
						50.631334434171784
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "LT_SUD"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.464968681335448,
						50.65057612253766
					],
					[
						5.427417755126953,
						50.626842665766056
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436429977416992,
						50.635295001161005
					],
					[
						5.4386186599731445,
						50.633934019209676
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436451435089111,
						50.63528139153653
					],
					[
						5.434219837188721,
						50.63666955293341
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.43424129486084,
						50.63665594370694
					],
					[
						5.435314178466797,
						50.637322791169645
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.42574405670166,
						50.633580157447675
					],
					[
						5.431365966796874,
						50.63480505219804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.428726673126221,
						50.63217829428814
					],
					[
						5.430421829223633,
						50.63462812492856
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.434219837188721,
						50.63665594370694
					],
					[
						5.431344509124755,
						50.63480505219804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.429520606994629,
						50.63091249843317
					],
					[
						5.428705215454102,
						50.63219190481106
					],
					[
						5.426366329193115,
						50.633743478591924
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.43248176574707,
						50.63277715356829
					],
					[
						5.437760353088379,
						50.63340322556774
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.431044101715088,
						50.62914304925794
					],
					[
						5.429971218109131,
						50.629701113495265
					],
					[
						5.429627895355225,
						50.630204726754755
					],
					[
						5.429542064666748,
						50.63091249843317
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.439155101776122,
						50.63200135713174
					],
					[
						5.438854694366455,
						50.63193330420196
					],
					[
						5.438361167907715,
						50.63206940996304
					],
					[
						5.438232421875,
						50.63234162030297
					],
					[
						5.438597202301025,
						50.63311741112414
					],
					[
						5.4387688636779785,
						50.633348784855315
					],
					[
						5.438575744628906,
						50.633934019209676
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.451879501342773,
						50.64001730292809
					],
					[
						5.449433326721191,
						50.63967709531815
					],
					[
						5.447523593902588,
						50.63956822836283
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.439133644104003,
						50.63200135713174
					],
					[
						5.451858043670654,
						50.640030911181256
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.4579949378967285,
						50.643841067055
					],
					[
						5.455033779144287,
						50.64394992411214
					],
					[
						5.454690456390381,
						50.64411320922499
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.4547119140625,
						50.644099602153915
					],
					[
						5.454390048980713,
						50.644412563791555
					],
					[
						5.454411506652832,
						50.645555536412644
					],
					[
						5.454132556915283,
						50.64637192840583
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.4467082023620605,
						50.63902388980346
					],
					[
						5.444519519805907,
						50.6403983325274
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.451858043670654,
						50.640030911181256
					],
					[
						5.4579949378967285,
						50.64385467420096
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.457973480224609,
						50.643841067055
					],
					[
						5.462007522583008,
						50.64641274763311
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.461986064910889,
						50.64642635403433
					],
					[
						5.466148853302002,
						50.64898428746218
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.462007522583008,
						50.646480779599834
					],
					[
						5.462136268615723,
						50.64675290648158
					],
					[
						5.462028980255126,
						50.647038638011146
					],
					[
						5.460655689239502,
						50.64788221619841
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S6"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.466127395629883,
						50.64897068180172
					],
					[
						5.466127395629883,
						50.649460483096135
					],
					[
						5.466041564941406,
						50.64961014358422
					],
					[
						5.464925765991211,
						50.65023599136891
					],
					[
						5.464410781860352,
						50.650249596662945
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.458402633666992,
						50.64642635403433
					],
					[
						5.456664562225341,
						50.64751485336572
					],
					[
						5.454754829406737,
						50.64826318202804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.444498062133788,
						50.6403983325274
					],
					[
						5.443897247314453,
						50.64079296706997
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_W2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436751842498779,
						50.63722752782565
					],
					[
						5.442395210266113,
						50.640752142960615
					],
					[
						5.442953109741211,
						50.64088822318721
					],
					[
						5.443553924560546,
						50.64088822318721
					],
					[
						5.443918704986572,
						50.64079296706997
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_E0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.443854331970215,
						50.640820183123175
					],
					[
						5.443618297576904,
						50.641092342788276
					],
					[
						5.443575382232666,
						50.641623049601776
					],
					[
						5.44771671295166,
						50.64571881594608
					],
					[
						5.451643466949463,
						50.64740600456736
					],
					[
						5.4531025886535645,
						50.647324367803115
					],
					[
						5.453660488128662,
						50.647038638011146
					],
					[
						5.454132556915283,
						50.64639914122797
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_NE"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.454111099243164,
						50.646439960431636
					],
					[
						5.453639030456543,
						50.646113405809366
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_NO"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.429520606994629,
						50.63091249843317
					],
					[
						5.430164337158203,
						50.631334434171784
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "LAND_23R_SHORT"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.438747406005859,
					50.63672398979986
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "LAND_23R_LONG"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.43398380279541,
					50.63370264835906
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P128"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.433769226074218,
					50.63754053523086
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P112"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.426945686340331,
					50.63479144243171
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P26"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.460129976272583,
					50.64376622768195
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "PM2"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.441837310791016,
					50.63264104985633
				]
			}
		}
	]
}
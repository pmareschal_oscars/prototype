var eblgTaxi = {
	"type": "FeatureCollection",
	"features": [
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "LT_NORD"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.453617572784424,
						50.64609979931754
					],
					[
						5.444573163986206,
						50.64037962132456
					],
					[
						5.430153608322143,
						50.63128339375958
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "LT_SUD"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.4649901390075675,
						50.650572721238156
					],
					[
						5.464432239532471,
						50.65022238607096
					],
					[
						5.460682511329651,
						50.64785500423525
					],
					[
						5.458383858203888,
						50.64640424363034
					],
					[
						5.454682409763336,
						50.64406898622961
					],
					[
						5.447496771812439,
						50.639532506338185
					],
					[
						5.446702837944031,
						50.63903069407439
					],
					[
						5.43861597776413,
						50.633932317957566
					],
					[
						5.4377952218055725,
						50.63340832938129
					],
					[
						5.431025326251984,
						50.62913454212986
					],
					[
						5.427417755126953,
						50.626842665766056
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436429977416992,
						50.635295001161005
					],
					[
						5.4386106133461,
						50.633934019209676
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436526536941528,
						50.635237160229856
					],
					[
						5.434281527996063,
						50.63666104716733
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.434284210205077,
						50.63666104716733
					],
					[
						5.4353517293930045,
						50.63731598665147
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.42574405670166,
						50.633580157447675
					],
					[
						5.426382422447205,
						50.6337077521401
					],
					[
						5.43037086725235,
						50.63453795981393
					],
					[
						5.431387424468993,
						50.63479144243171
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.428699851036072,
						50.632157878496386
					],
					[
						5.43037086725235,
						50.63453795981393
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_D1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.434284210205077,
						50.63665934601392
					],
					[
						5.431390106678009,
						50.63479144243171
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.429566204547882,
						50.630869964378434
					],
					[
						5.429037809371948,
						50.63179719804682
					],
					[
						5.428702533245087,
						50.632157878496386
					],
					[
						5.42637974023819,
						50.6337077521401
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C1"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.43248176574707,
						50.63277715356829
					],
					[
						5.437792539596558,
						50.63340832938129
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.431025326251984,
						50.62913284070407
					],
					[
						5.430488884449005,
						50.6293812482183
					],
					[
						5.430083870887756,
						50.62961264033853
					],
					[
						5.429812967777252,
						50.62989337189652
					],
					[
						5.429593026638031,
						50.6302234420084
					],
					[
						5.4295554757118225,
						50.63087166574135
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.439173877239227,
						50.63197923994035
					],
					[
						5.438873469829559,
						50.63190608300246
					],
					[
						5.438626706600189,
						50.63194010949937
					],
					[
						5.438361167907715,
						50.63206940996304
					],
					[
						5.438288748264313,
						50.63212895610957
					],
					[
						5.438248515129089,
						50.63233651637359
					],
					[
						5.438599884510039,
						50.63302724311169
					],
					[
						5.438739359378815,
						50.63332326574969
					],
					[
						5.43861597776413,
						50.63393572046171
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.451879501342773,
						50.64000369467097
					],
					[
						5.449433326721191,
						50.63967709531815
					],
					[
						5.447491407394409,
						50.639530805288715
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.439176559448241,
						50.63197923994035
					],
					[
						5.451879501342773,
						50.64000369467097
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.457978844642639,
						50.64383936616151
					],
					[
						5.455103516578673,
						50.64392781254283
					],
					[
						5.454679727554321,
						50.64406898622961
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.454679727554321,
						50.64407068711484
					],
					[
						5.454432964324951,
						50.64444998297827
					],
					[
						5.454411506652832,
						50.645555536412644
					],
					[
						5.454132556915283,
						50.64637192840583
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.446702837944031,
						50.639028993006754
					],
					[
						5.444570481777191,
						50.64037962132456
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.451879501342773,
						50.64000369467097
					],
					[
						5.457984209060669,
						50.643841067055
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A4"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.457981526851653,
						50.64383936616151
					],
					[
						5.462002158164978,
						50.64638213321595
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_A5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.462007522583008,
						50.64637873161283
					],
					[
						5.46593427658081,
						50.64886523779951
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.462007522583008,
						50.64637533000944
					],
					[
						5.462104082107544,
						50.646555614649415
					],
					[
						5.462136268615723,
						50.64675290648158
					],
					[
						5.462093353271484,
						50.64689237089762
					],
					[
						5.462007522583008,
						50.64702503178725
					],
					[
						5.460687875747681,
						50.64785160273873
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S6"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.465931594371796,
						50.648866938511105
					],
					[
						5.466119349002837,
						50.64906081922888
					],
					[
						5.466172993183136,
						50.64918667119637
					],
					[
						5.46618103981018,
						50.64932442769324
					],
					[
						5.466127395629883,
						50.649460483096135
					],
					[
						5.465969145298004,
						50.649645857948414
					],
					[
						5.465183258056641,
						50.65012544820887
					],
					[
						5.464925765991211,
						50.65022238607096
					],
					[
						5.464432239532471,
						50.650225787395804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_C5"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.458383858203888,
						50.64640254282962
					],
					[
						5.456664562225341,
						50.64751485336572
					],
					[
						5.454754829406737,
						50.64826318202804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_N3"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.444570481777191,
						50.64037621928689
					],
					[
						5.444211065769195,
						50.64058714515735
					],
					[
						5.44397234916687,
						50.64072322586171
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_W2"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.436902046203613,
						50.63726835499671
					],
					[
						5.442376434803009,
						50.640733431898646
					],
					[
						5.442735850811005,
						50.64083549214618
					],
					[
						5.443004071712488,
						50.64087291418138
					],
					[
						5.4434627294540405,
						50.64085930617202
					],
					[
						5.44397234916687,
						50.640721524855344
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_E0"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.443975031375885,
						50.640721524855344
					],
					[
						5.443634390830993,
						50.6410294060058
					],
					[
						5.443575382232666,
						50.641623049601776
					],
					[
						5.44771671295166,
						50.64571881594608
					],
					[
						5.451761484146118,
						50.6473855953896
					],
					[
						5.453166961669922,
						50.64727334475341
					],
					[
						5.453660488128662,
						50.64706585044717
					],
					[
						5.45420229434967,
						50.646320904321804
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_NE"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.45421838760376,
						50.64637192840583
					],
					[
						5.453590750694275,
						50.64609299607011
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_NO"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.429560840129852,
						50.63087166574135
					],
					[
						5.430153608322143,
						50.63128339375958
					]
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "LAND_23R_SHORT"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.438747406005859,
					50.63672398979986
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "LAND_23R_LONG"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.43398380279541,
					50.63370264835906
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P128"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.433769226074218,
					50.63754053523086
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P112"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.426945686340331,
					50.63479144243171
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "P26"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.460129976272583,
					50.64376622768195
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"marker-color": "#7E7E7E",
				"marker-size": "medium",
				"marker-symbol": "",
				"name": "PM2"
			},
			"geometry": {
				"type": "Point",
				"coordinates": [
					5.441837310791016,
					50.63264104985633
				]
			}
		},
		{
			"type": "Feature",
			"properties": {
				"stroke": "#555555",
				"stroke-width": 2,
				"stroke-opacity": 1,
				"name": "TX_S2b"
			},
			"geometry": {
				"type": "LineString",
				"coordinates": [
					[
						5.437781810760498,
						50.63341003065236
					],
					[
						5.438066124916076,
						50.63342534208915
					],
					[
						5.438202917575836,
						50.633348784855315
					],
					[
						5.438438951969147,
						50.63326712366849
					],
					[
						5.438599884510039,
						50.63302724311169
					]
				]
			}
		}
	]
}
/* Ajouter à map:
center
zoom

Ajouter à map_layer
visible (boolean)

*/
/* create device groups */
insert into core_device_groups(name, display_name, device_type, status) values ('AIRCRAFTS', 'Aircrafts', null, 'ACTIVE');
insert into core_device_groups(name, display_name, device_type, status) values ('SNOWVEH', 'Snow Vehicles', 'SNOW', 'ACTIVE');
insert into core_device_groups(name, display_name, device_type, status) values ('TETRA', 'Tetra Phones', 'TETRA', 'ACTIVE');
insert into core_device_groups(name, display_name, device_type, status) values ('GROUND', 'Ground Vehicles', null, 'ACTIVE');
;

/* create zones */
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('23L5R','23L/5R','TRACK','ACTIVE','ACTIVE', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.453488826751709,50.64616783173735,5.42999267578125,50.631388877217056,5.4302287101745605,50.63122554789211,5.4537034034729,50.646031766799176,5.453488826751709,50.64616783173735)));
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('5L23R','5L/23R','TRACK','INACTIVE','INACTIVE', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.464754104614258,50.65065775365173,5.427074432373047,50.62695156219702,5.4274606704711905,50.62676099327735,5.465140342712402,50.6504672808316,5.464754104614258,50.65065775365173)));
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('P27','27','APRON','ACTIVE','BUSY', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.460282862186431,50.64341754266795,5.459751784801483,50.64375602230368,5.4594942927360535,50.643592735949625,5.460028052330017,50.64324745147831,5.460282862186431,50.64341754266795)));
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('P26','26','APRON','ACTIVE','FREE', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.460438430309307,50.64352129799132,5.4606825113296695,50.64367778099649,5.4601326584816094,50.64400605343367,5.459901988506329,50.64385297330792,5.460438430309307,50.64352129799132)));
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('P25','25','APRON','ACTIVE','FREE', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.4608649015426876,50.6437985446986,5.461100935935997,50.64394992411212,5.460551083087944,50.644281596403324,5.460312366485621,50.64412681629209,5.4608649015426876,50.6437985446986)));
insert into core_zones(name, display_name, zone_type, status, display_status, geom2d) values ('P24','24','APRON','ACTIVE','FREE', MDSYS.SDO_GEOMETRY(2003,4326,null, MDSYS.SDO_ELEM_INFO_ARRAY(1,1003,1),MDSYS.SDO_ORDINATE_ARRAY(5.461275279521941,50.64407408888506,5.461500585079191,50.64422716829048,5.460950732231138,50.644552035154256,5.460712015628814,50.644398956807144,5.461275279521941,50.64407408888506)));
/* create empty groups */
insert into core_zone_groups(name, display_name, zone_type, status) values ('LANDINGTRACKS', 'Landing Tracks', null, 'ACTIVE');
insert into core_zone_groups(name, display_name, zone_type, status) values ('PARKINGS', 'Parkings', 'PARKING', 'ACTIVE');
commit;
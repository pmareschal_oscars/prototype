var fs = require('fs');
var geojsonArea = require('@mapbox/geojson-area');

fs.readFile('./samplehouses.json', 'utf8', function (err, data) {
    if (err) throw err;
    var obj = JSON.parse(data);
	obj.features.forEach(function(f) {
		console.log(f.properties.name, geojsonArea.geometry(f.geometry))
	})
});
//https://stackoverflow.com/questions/32482608/withrelated-binding-is-undefined
var Bookshelf = require('bookshelf');
var knex = require('knex')(require('../config/121test.js'))
Bookshelf.DB = Bookshelf(knex);


var Zones = require('./collections/zones').collection;

var proc = setTimeout(knex.destroy, 10000)

var promise = new Zones().orderBy('NAME', 'DESC').fetch().then(function(collection) {
	console.log(collection.toJSON())
	clearTimeout(proc)
	knex.destroy()
}, function() {
	knex.destroy()
});
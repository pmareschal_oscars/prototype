//https://stackoverflow.com/questions/32482608/withrelated-binding-is-undefined
var Bookshelf = require('bookshelf');
var knex = require('knex')(require('../config/121test'))
Bookshelf.DB = Bookshelf(knex);


var DeviceGroups = require('./collections/deviceGroups').collection;

var proc = setTimeout(knex.destroy, 10000)

var promise = new DeviceGroups().orderBy('NAME', 'DESC').fetch({
		withRelated: ['devices']
	}).then(function(collection) {
	console.log(collection.toJSON())
	clearTimeout(proc)
	knex.destroy()
}, function() {
	knex.destroy()
});
// bring in Bookshelf!
var Bookshelf = require('bookshelf');

var knex = require('knex')({
	client: 'oracledb',
	connection: {
		user          : "gipcoretest",
		password      : "Oz4rsoz4rs#",
		//connectString : '37.59.208.82/oscars',
		//externalAuth  : false,
		host          : "37.59.208.82",
		database      : "oscars"
	},
	fetchAsString: [ 'number', 'clob' ],
	debug: true
})

Bookshelf.DB = Bookshelf(knex);

var Devices = require('./collections/devices').collection;

new Devices().fetch().then(function(collection) {
	console.log(collection.toJSON())
	knex.destroy()
});

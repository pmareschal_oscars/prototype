/* 
 * This represents a collection of all users in the Users table. We only really
 * use this for our list method.
 */

var Bookshelf = require('bookshelf').DB;

var Zone = require("../models/zone").model;

exports.collection = Bookshelf.Collection.extend({
	model: Zone
});
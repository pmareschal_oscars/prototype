/* 
 * This represents a collection of all users in the Users table. We only really
 * use this for our list method.
 */

var Bookshelf = require('bookshelf').DB;

var Rule = require("../models/rule").model;

exports.collection = Bookshelf.Collection.extend({
	model: Rule
});
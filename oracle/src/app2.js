// bring in Bookshelf!
var Bookshelf = require('bookshelf');

var knex = require('knex')({
	client: 'oracledb',
	connection: {
		user          : "gipcoretest",
		password      : "Oz4rsoz4rs#",
		//connectString : '37.59.208.82/oscars',
		//externalAuth  : false,
		host          : "37.59.208.82",
		database      : "oscars"
	},
	fetchAsString: [ 'number', 'clob' ],
	debug: true
})

Bookshelf.DB = Bookshelf(knex);

var Rules = require('./collections/rules').collection;

var proc = setTimeout(knex.destroy, 10000)

new Rules().fetch({
		withRelated: ['service']
	}).then(function(collection) {
	console.log(collection.toJSON())
	clearTimeout(proc)
	knex.destroy()
});



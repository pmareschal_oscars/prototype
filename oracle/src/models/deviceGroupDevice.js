var Bookshelf = require('bookshelf').DB;

var Device = require("./device").model;
var DeviceGroup = require("./deviceGroup").model;

exports.model = Bookshelf.Model.extend({
	tableName: "CORE_DEVICE_IN_GROUPS",
	idAttribute: "ID",

	device: function() {
		return this.hasOne(Device, "device_id", "id");
	},
	
	deviceGroup: function() {
		return this.hasOne(DeviceGroup, "device_group_id", "id");
	}
});
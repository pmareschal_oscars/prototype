var Bookshelf = require('bookshelf').DB;

var Device = require("./device").model;

exports.model = Bookshelf.Model.extend({
	tableName: "CORE_DEVICE_GROUPS",
	idAttribute: "ID",

	devices: function() {
		return this.belongsToMany(Device, "CORE_DEVICE_IN_GROUPS", "DEVICE_GROUP_ID", "DEVICE_ID" )
	}
	
});
var Bookshelf = require('bookshelf').DB;

var ZoneGroup = require("./zoneGroup").model;

exports.model = Bookshelf.Model.extend({
	tableName: "ZONES_V",
	idAttribute: "ID",
	
	groups: function() {
		return this.belongsToMany(ZoneGroup, "CORE_ZONE_IN_GROUPS", "ZONE_ID", "ZONE_GROUP_ID" )
	}
	
});
var Bookshelf = require('bookshelf').DB;

var Device = require("./zone").model;

exports.model = Bookshelf.Model.extend({
	tableName: "ZONE_GROUPS",
	idAttribute: "ID",

	zones: function() {
		return this.belongsToMany(Device, "CORE_ZONE_IN_GROUPS", "ZONE_GROUP_ID", "ZONE_ID" )
	}
	
});
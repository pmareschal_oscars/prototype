var Bookshelf = require('bookshelf').DB;

var Rule = require("./rule").model;

exports.model = Bookshelf.Model.extend({
	tableName: "CORE_SERVICES",
	idAttribute: "ID",
	
	rules: function() {
		return this.hasMany(Rule, "SERVICE_ID");
	}
});
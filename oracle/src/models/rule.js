var Bookshelf = require('bookshelf').DB;

var Service = require("./service").model;

exports.model = Bookshelf.Model.extend({
	tableName: "CORE_RULES",
	idAttribute: "ID",
	
	service: function() {
		return this.belongsTo(Service, "SERVICE_ID");
	}
});
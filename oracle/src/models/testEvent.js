var Bookshelf = require('bookshelf').DB;

var TestEvent = require("./testEvent").model;

exports.model = Bookshelf.Model.extend({
	tableName: "TEST_EVENTS",
	idAttribute: "ID"
		
});
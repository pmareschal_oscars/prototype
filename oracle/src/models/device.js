var Bookshelf = require('bookshelf').DB;

var DeviceGroup = require("./deviceGroup").model;

exports.model = Bookshelf.Model.extend({
	tableName: "CORE_DEVICES",
	idAttribute: "ID",
	
	groups: function() {
		return this.belongsToMany(DeviceGroup, "CORE_DEVICE_IN_GROUPS", "DEVICE_ID", "DEVICE_GROUP_ID" )
	}
	
});
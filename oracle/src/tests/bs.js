
var User = bookshelf.Model.extend({
   tableName: 'users'
});

new User({id: 1}).fetch().then(function(user) {
  console.log(JSON.stringify(user)); // user with `id` of 1
})

User.collection().fetch().then(function(users) {
   console.log(JSON.stringify(users)); // collection of users
})
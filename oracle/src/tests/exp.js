const fs = require('fs')
const commandLineArgs = require('command-line-args')
var oracledb = require('oracledb');
var dbConfig = require('../dbconfig-gip.js');

const options = commandLineArgs([
	{ name: 'device', alias: 'd', multiple: true},
/*	{ name: 'device-group', alias: 'dg', multiple: true},
	{ name: 'zone', alias: 'z', multiple: true},
	{ name: 'zone-group', alias: 'zg', multiple: true},
	{ name: 'detection-type', alias: 'dt', multiple: true},
	{ name: 'detection', alias: 'de', multiple: true},
	{ name: 'provider-type', alias: 'pt', multiple: true},
	{ name: 'provider', alias: 'p', multiple: true},
	{ name: 'rule', alias: 'r', multiple: true},
	{ name: 'service', alias: 's', multiple: true},
	{ name: 'map', alias: 'm', multiple: true},
	{ name: 'layer', alias: 'ml', multiple: true},
	{ name: 'layer-type', alias: 'mlt', multiple: true},
	{ name: 'user', alias: 'u', multiple: true},
	{ name: 'role', alias: 'r', multiple: true},
	{ name: 'privilege', alias: 'priv', multiple: true},*/
	{ name: 'mode', default: "nested" },
])
console.log(options)

function lco(obj) {
	var key, keys = Object.keys(obj);
	var n = keys.length;
	var newobj={}
	while (n--) {
	  key = keys[n];
	  newobj[key.toLowerCase()] = obj[key];
	}
	return newobj;
}


var sql, prm = {};

for (var option in options) {
    if (options.hasOwnProperty(option)) {
			switch(option) {
				case 'device':
				  for(var i = 0; i < options.device.length; i++) {
					  prm[i] = options.device[i]
				  }
					sql = "SELECT * "+
					"FROM core_devices "+
					"WHERE NAME in (:"+Object.keys(prm).join(',:')+")"
					break;
			}
    }
}

// Get a non-pooled connection
oracledb.getConnection(
  {
    user          : dbConfig.user,
    password      : dbConfig.password,
    connectString : dbConfig.connectString
  },
  function(err, connection)
  {
    if (err) {
      console.error(err.message);
      return;
    }
    connection.execute(
      // The statement to execute
			sql,

      // The "bind value" 180 for the "bind variable" :id
      prm,

      // Optional execute options argument, such as the query result format
      // or whether to get extra metadata
      { outFormat: oracledb.OBJECT, extendedMetaData: false },

      // The callback function handles the SQL execution results
      function(err, result)
      {
        if (err) {
          console.error(err.message);
          doRelease(connection);
          return;
        }
//        console.log(result.metaData); // [ { name: 'DEPARTMENT_ID' }, { name: 'DEPARTMENT_NAME' } ]
//				console.log(result.rows);     // [ [ 180, 'Construction' ] ]
				result.rows.forEach(function(r) {
					var n = lco(r)
					console.log(n)
				})
        doRelease(connection);
      });
  });

// Note: connections should always be released when not needed
function doRelease(connection)
{
  connection.close(
    function(err) {
      if (err) {
        console.error(err.message);
      }
    });
}

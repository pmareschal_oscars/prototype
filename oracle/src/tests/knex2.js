const version = '1.0.0'
const commandLineArgs = require('command-line-args')

const structure = {
	device: {table_name: 'core_devices'},
	service: {
		table_name: 'core_services',
		hasMany: [
			['rule', 'id', 'service_id']
		]
	},
	zone: {table_name: 'core_zones'},
	attribute_type: {table_name: 'broker_attribute_types'},
	attribute: {table_name: 'broker_attributes'},
	device_group: {
		table_name: 'core_device_groups',
		hasMany: [
			['device', 'id', 'device_group_id', 'core_device_group_devices', 'device_id']
		]
	},
	zone_group: {table_name: 'core_zone_groups'},
	notification_group: {table_name: 'core_notification_groups'},
	detection: {table_name: 'core_detections'},
	detection_type: {table_name: 'core_detection_types'},
	rule: {table_name: 'core_rules'},
	provider: {table_name: 'broker_providers'},
	provider_type: {table_name: 'broker_provider_types'},
	notification: {table_name: 'core_notifications'},
	notification_type: {table_name: 'core_notification_types'},
	subscription: {table_name: 'core_subscriptions'},
	map: {table_name: 'maps'},
	layer: {table_name: 'map_layers'},
	layer_type: {table_name: 'layer_types'},
	style: {table_name: 'styles'},
	version: {table_name: 'gip_versions'}
}

var commandLineOptions = [
	{ name: 'mode', default: "nested" },
	{ name: 'debug', type: Boolean},
	{ name: 'list', multiple: true}
]

for(var entity in structure) {
	if(structure.hasOwnProperty(entity)) {
		commandLineOptions.push({
			name: entity,
			multiple: true
		})
	}
}

const options = commandLineArgs(commandLineOptions)

if(options.debug) {
	console.log(options)
}

var exports = {
	version: version,
	blocks: []
}

var knex = require('knex')({
	client: 'oracledb',
	connection: {
		user          : "gipcoretest",
		password      : "Oz4rsoz4rs#",
		//connectString : '37.59.208.82/oscars',
		//externalAuth  : false,
		host          : "37.59.208.82",
		database      : "oscars"
	},
	fetchAsString: [ 'number', 'clob' ],
	debug: options.debug
})

function lco(obj) {
	var key, keys = Object.keys(obj);
	var n = keys.length;
	var newobj={}
	while (n--) {
	  key = keys[n];
	  newobj[key.toLowerCase()] = obj[key];
	}
	return newobj;
}

function fetchOne(entity, next) {
	var id = next.pop()
	if(! id)
		return;

	if(id == '*') {
		knex.select('*')
			.from(structure[entity].table_name.toUpperCase())
			.asCallback(function(err, values) {
				if(err) {
					console.log(err);
				} else {
					values.forEach(function(device){
						console.log(lco(device))
					})
				}
			});
	} else {
		var col = (typeof id === "string" || id instanceof String) ? 'NAME' : 'ID'
		var val = knex.select('*')
			.from(structure[entity].table_name.toUpperCase())
			.where(col, id)
			.asCallback(function(err, values) {
				if(err) {
					console.log(err);
				} else {
					values.forEach(function(device){
						console.log(lco(device))
					})
				}
			});
	}
	return fetchOne(entity, next)
}

function listOne(entity, next) {
	if(Object.keys(structure).indexOf(entity) >= 0 && structure[entity].table_name) {
		knex.select('NAME')
			.from(structure[entity].table_name.toUpperCase())
			.orderBy('NAME')
			.asCallback(function(err, values) {
				if(err) {
					console.log(err);
				} else {
					console.log('>>> ', entity)
					if(values.length == 0)
						console.log("no data found")
					else
					values.forEach(function(obj){
						console.log(obj.NAME)
					})
					console.log('---')
				}
			});
	}
	if(next.length > 0) {
		var nent = next.pop();
		listOne(nent, next);
	}
}

if(options.list) {
	var ent = options.list.pop()
	listOne(ent, options.list)
} else {
	for(var entity in options) {
		if (options.hasOwnProperty(entity) && Object.keys(structure).indexOf(entity) >= 0 && structure[entity].table_name) {
			exports.blocks[entity] = []
			fetchOne(entity, options[entity])
		}
	}
}
console.log(exports)
knex.destroy()
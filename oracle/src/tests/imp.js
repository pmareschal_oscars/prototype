const fs = require('fs')
const commandLineArgs = require('command-line-args')
var oracledb = require('oracledb');
var dbConfig = require('../dbconfig.js');

const options = commandLineArgs([
  { name: 'files', alias: 'f', multiple: true  },
  { name: 'existing', default: 'create' }
])
//console.log(JSON.stringify(options, null, 2))

options.files.forEach(function(file) {
	console.log(file)
})

// Get a non-pooled connection
oracledb.getConnection(
  {
    user          : dbConfig.user,
    password      : dbConfig.password,
    connectString : dbConfig.connectString
  },
  function(err, connection)
  {
    if (err) {
      console.error(err.message);
      return;
    }
    connection.execute(
      // The statement to execute
			"SELECT json_object('name' VALUE nomacces, 'hasCommission' VALUE "+
			"CASE WHEN blnvisible IS NULL THEN 'false' ELSE 'true' "+
			"END FORMAT JSON) "+
			"FROM acces",

      // The "bind value" 180 for the "bind variable" :id
      [],

      // Optional execute options argument, such as the query result format
      // or whether to get extra metadata
      // { outFormat: oracledb.OBJECT, extendedMetaData: true },

      // The callback function handles the SQL execution results
      function(err, result)
      {
        if (err) {
          console.error(err.message);
          doRelease(connection);
          return;
        }
        console.log(result.metaData); // [ { name: 'DEPARTMENT_ID' }, { name: 'DEPARTMENT_NAME' } ]
        console.log(result.rows);     // [ [ 180, 'Construction' ] ]
        doRelease(connection);
      });
  });

// Note: connections should always be released when not needed
function doRelease(connection)
{
  connection.close(
    function(err) {
      if (err) {
        console.error(err.message);
      }
    });
}

{
	device: {table_name: 'core_devices'},
	zone: {table_name: 'code_zones'},
	parameter: {table_name: '','parameter_type'},
	parameter_type: {table_name: ''},
	device_group: {table_name: '',['device']},
	zone_group: {table_name: '',['zone']},
	notification_group: {table_name: '',['notification']},
	detection: {table_name: '',{'detection_type',['parameter']},
	detection_type: {table_name: '',['parameter']},
	rule: {table_name: '',{'service', 'detection','notification_group'}},
	provider: {table_name: '',{'provider_type',['parameter']},
	provider_type: {table_name: '',['parameter']},
	notification: {table_name: '',{'notification_type',['parameter']},
	notification_type: {table_name: '',['parameter']},
	service: {table_name: '',['rule']},
	subscription: {table_name: '',{'service','device_group','provider'}},
	map:['layer'],
	layer: {table_name: '',{'layer_type', ['style']}},
	layer_type: {table_name: ''},
	style: {table_name: ''}
}



const commandLineArgs = require('command-line-args')
const options = commandLineArgs([
	{ name: 'devices', multiple: true},
/*	{ name: 'device-group', multiple: true},
	{ name: 'zone', multiple: true},
	{ name: 'zone-group', multiple: true},
	{ name: 'detection-type', multiple: true},
	{ name: 'detection', multiple: true},
	{ name: 'provider-type', multiple: true},
	{ name: 'provider', multiple: true},
	{ name: 'rule', multiple: true},
	{ name: 'service', multiple: true},
	{ name: 'map', multiple: true},
	{ name: 'layer', multiple: true},
	{ name: 'layer-type', multiple: true},
	{ name: 'user', multiple: true},
	{ name: 'role', multiple: true},
	{ name: 'privilege', multiple: true},*/
	{ name: 'mode', default: "nested" },
	{ name: 'debug', type: Boolean}
])
if(options.debug) {
	console.log(options)
}

var knex = require('knex')({
  client: 'oracledb',
  connection: {
	      user          : "gipcoretest",
	      password      : "Oz4rsoz4rs#",
	      host          : "37.59.208.82",
	      database      : "oscars"
	},
  fetchAsString: [ 'number', 'clob' ],
debug: true
})


knex.select('*')
	.from('CORE_DEVICES')
	.whereIn('NAME', options.devices)
	.asCallback(function(err, values) {
		if(err) {
			console.log(err);
		} else {
			console.log(values);
		}
		knex.destroy();
	});

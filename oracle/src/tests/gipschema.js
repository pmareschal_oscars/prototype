module.exports = {

broker_attributes: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	attribute_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

broker_attribute_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	data_type: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	list_of_values_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

broker_attribute_values: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	entity_attribute_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	entity_type: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	entity_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	value_string: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	value_number: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	value_date: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
},

broker_entity_attributes: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	attribute_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	entity_type: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	entity_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	mandatory: {type: 'string', maxlength:1, nullable:true, fieldtype:"varchar2"},
	default_value: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	array_index: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

broker_entity_attributes_v: {
	id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	entity_type: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	entity_name: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	attribute_name: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	attribute_type: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
},

broker_events: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	event_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

broker_event_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

broker_list_of_values: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	data_type: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	list_type: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	table_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	value_column: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	display_column: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
},

broker_lov_values: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	list_of_values_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

broker_mappings: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	processing_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	input_attribute_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	input_format: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	output_attribute_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	output_format: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

broker_processing_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
},

broker_processing_mappings_v: {
	processing_group_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	processing_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	in_event_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	in_attribute_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	out_event_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	out_attribute_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
},

broker_processings: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	processing_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	provider_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	target_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	enabled: {type: 'string', maxlength:1, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

broker_processings_v: {
	processing_group_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	processing_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	provider_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	provider_type_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	input_event_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	target_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	target_type_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	output_event_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
},

broker_provider_attrval_v: {
	provider_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	attribute_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	attribute_value_string: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	attribute_value_number: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	attribute_value_date: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
},

broker_providers: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	provider_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	event_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

broker_provider_type_attr_v: {
	provider_type_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	attribute_name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	attribute_type: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
},

broker_provider_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
},

broker_targets: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	target_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	event_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

broker_target_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
},

core_detection_categories: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

core_detection_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	detection_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	detection_category_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

core_device_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	device_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_status_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

core_device_in_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	device_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	device_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

core_devices: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	device_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

core_display_statuses: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	display_status_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	style_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

core_display_status_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

core_notification_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

core_notification_in_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	notification_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	notification_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

core_notifications: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	notification_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

core_notification_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

core_rules: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	detection_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	service_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	notification_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

core_services: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

core_styles: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	marker: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_opacity: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	marker_size: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_opacity: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	fill_pattern: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_opacity: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	stroke_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

core_subscriptions: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	is_trusted: {type: 'string', maxlength:1, nullable:true, fieldtype:"varchar2"},
	service_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	device_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	provider_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	source_name: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

core_zone_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	zone_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_status_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

core_zone_in_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	zone_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	zone_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

core_zones: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	geom2d: {type: "sdo_geometry", maxlength:1, nullable:true, fieldtype:"sdo_geometry"},
	geom3d: {type: "sdo_geometry", maxlength:1, nullable:true, fieldtype:"sdo_geometry"},
	zone_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

customer: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	birthdate: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	email: {type: 'string', maxlength:1020, nullable:true, fieldtype:"varchar2"},
	firstname: {type: 'string', maxlength:1020, nullable:true, fieldtype:"varchar2"},
	gender: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	lastname: {type: 'string', maxlength:1020, nullable:true, fieldtype:"varchar2"},
	location: {type: "blob", maxlength:4000, nullable:true, fieldtype:"blob"},
	status: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	version: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

device_display_styles: {
	device_group_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	device_group_display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	device_group_description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	device_group_status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	layer_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	layer_status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	template_show_label: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	template_text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_sidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_symbol: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stoke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

device_display_styles_v: {
	device_group_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	layer_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	template_show_label: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	template_text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_sidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	marker_symbol: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stoke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

display_styles_v: {
	group_type: {type: 'string', maxlength:6, nullable:true, fieldtype:"varchar2"},
	group_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	template_show_label: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	template_text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_sidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	marker_symbol: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stoke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

external_layer: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	external_layer_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

external_layer_type: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

gip_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

gip_layers: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	group_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	group_id: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	gip_cluster: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	dynamic: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	selectable: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	showlabel: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	text_slidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	link_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	jp_marker_orientation: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
	jp_identifier: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
},

gip_maps: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	group_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	group_id: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	show_zoom: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	show_scale: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	show_layers: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	init_zoom: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	init_center: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
},

gip_preferences: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

gip_strings: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	domain: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	subdomain: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	lang: {type: 'string', maxlength:4, nullable:true, fieldtype:"varchar2"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

gip_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

gip_user_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	user_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	group_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

gip_user_preferences: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	user_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	preference_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	value_text: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	value_number: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

gip_user_profiles: {
	user_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	phone_number: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	email: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	location: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	position: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	website: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

gip_users: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	password: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	lang: {type: 'string', maxlength:4, nullable:true, fieldtype:"varchar2"},
	token: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

groupmembers: {
	g_name: {type: 'string', maxlength:200, nullable:false, fieldtype:"varchar2"},
	g_member: {type: 'string', maxlength:200, nullable:false, fieldtype:"varchar2"},
},

groups: {
	g_name: {type: 'string', maxlength:200, nullable:false, fieldtype:"varchar2"},
	g_description: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
},

holifresh_log_detections: {
	device_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	sended_date: {type: "timestamp(6) with time zone", maxlength:13, nullable:true, fieldtype:"timestamp(6) with time zone"},
	received_date: {type: "timestamp(6) with time zone", maxlength:13, nullable:true, fieldtype:"timestamp(6) with time zone"},
	rule_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	ozengine_process_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	acceleration: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	longitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	latitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	altitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	gip_json: {type: "clob", maxlength:4000, nullable:true, fieldtype:"clob"},
	service_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	temperature: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

log_detections: {
	device_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	sended_date: {type: "timestamp(6) with time zone", maxlength:13, nullable:true, fieldtype:"timestamp(6) with time zone"},
	received_date: {type: "timestamp(6) with time zone", maxlength:13, nullable:true, fieldtype:"timestamp(6) with time zone"},
	rule_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	ozengine_process_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	gip_json: {type: "clob", maxlength:4000, nullable:true, fieldtype:"clob"},
	service_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	temperature: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	latitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	longitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	altitude: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

map_layer: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	map_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_id: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	layer_group_name: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	zoom_min: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	zoom_max: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

oz_bind_values: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	bind_value_varchar2: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	bind_value_number: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	bind_value_clob: {type: "clob", maxlength:4000, nullable:true, fieldtype:"clob"},
	bind_value_date: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	bind_value_rowid: {type: "rowid", maxlength:10, nullable:true, fieldtype:"rowid"},
	bind_value_blob: {type: "blob", maxlength:4000, nullable:true, fieldtype:"blob"},
	bind_value_sdo_geometry: {type: "sdo_geometry", maxlength:1, nullable:true, fieldtype:"sdo_geometry"},
},

oz_conditions: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	nom_condition: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
	operator_value: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_events: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	event_name: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_events_columns: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	column_name: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
	column_type: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_external_services: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	service_type: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

oz_external_services_params: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	id_oes: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	param_name: {type: 'string', maxlength:100, nullable:true, fieldtype:"varchar2"},
	param_value: {type: 'string', maxlength:100, nullable:true, fieldtype:"varchar2"},
	param_type: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_operand: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	operand_value: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	oqt_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oc_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	operand_type: {type: 'string', maxlength:100, nullable:true, fieldtype:"varchar2"},
	parent_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	format: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	operand_side: {type: 'string', maxlength:1, nullable:true, fieldtype:"varchar2"},
},

oz_operand_bind_values: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oo_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	obv_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	group_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

oz_queries: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	query: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	query_name: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	query_type: {type: 'string', maxlength:10, nullable:true, fieldtype:"varchar2"},
},

oz_queries_columns: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	column_name: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
	column_type: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_queries_result: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	info: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
	etat: {type: 'string', maxlength:1, nullable:true, fieldtype:"varchar2"},
	geom: {type: "sdo_geometry", maxlength:1, nullable:true, fieldtype:"sdo_geometry"},
	q_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	run_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

oz_queries_run: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	obv_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oq_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	group_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	format: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:4000, nullable:true, fieldtype:"varchar2"},
},

oz_queries_tab_cols: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oq_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oqc_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oqt_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oec_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oe_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oes_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	exec_order: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

oz_queries_tables: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	table_owner: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
	table_name: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
},

oz_queries_tab_params: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oq_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oqwc_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

oz_queries_tabs: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	oq_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	oqt_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

oz_queries_where_clause: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	order_nb: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	group_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	id_left_cond: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	id_right_cond: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	logical_operator_value: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
	group_type: {type: 'string', maxlength:10, nullable:true, fieldtype:"varchar2"},
},

oz_web_services: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	id_oes: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	url: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	method: {type: 'string', maxlength:50, nullable:true, fieldtype:"varchar2"},
	namespace: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
},

sequence: {
	seq_name: {type: 'string', maxlength:200, nullable:false, fieldtype:"varchar2"},
	seq_count: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

status_styles: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	layer_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	created_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	updated_at: {type: "date", maxlength:7, nullable:true, fieldtype:"date"},
	created_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	updated_by: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	marker_icon: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	poly_stroke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	poly_stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	poly_fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	poly_fill_pattern: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

users: {
	u_name: {type: 'string', maxlength:200, nullable:false, fieldtype:"varchar2"},
	u_password: {type: 'string', maxlength:50, nullable:false, fieldtype:"varchar2"},
	u_description: {type: 'string', maxlength:1000, nullable:true, fieldtype:"varchar2"},
},

viewer_backgrounds: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	background_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_background_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

viewer_dashboard_giplets: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	rownumber: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	dashboard_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	giplet_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_dashboards: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	rowset: {type: 'string', maxlength:40, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

viewer_giplets: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	icon: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	giplet_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_giplet_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

viewer_layers: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	layer_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_layer_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

viewer_map_backgrounds: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	map_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	background_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_map_layers: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	layer_group: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	default_set: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	map_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	layer_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_maps: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

viewer_map_tool_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	map_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	tool_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_tool_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	tool_type_id: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
},

viewer_tool_in_groups: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	position: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	tool_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	tool_group_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_tools: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	icon: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	tool_type_id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
},

viewer_tool_types: {
	id: {type: "number", maxlength:22, nullable:false, fieldtype:"number"},
	name: {type: 'string', maxlength:40, nullable:false, fieldtype:"varchar2"},
	display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
},

zone_display_styles: {
	zone_group_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	zone_group_display_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	zone_group_description: {type: 'string', maxlength:200, nullable:true, fieldtype:"varchar2"},
	zone_group_status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	layer_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_display_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	layer_description: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	layer_status: {type: 'string', maxlength:20, nullable:true, fieldtype:"varchar2"},
	template_show_label: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	template_text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_sidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_symbol: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stoke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
},

zone_display_styles_v: {
	zone_group_name: {type: 'string', maxlength:80, nullable:false, fieldtype:"varchar2"},
	layer_name: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	styleset: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_type: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	object_status: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	template_show_label: {type: "number", maxlength:22, nullable:true, fieldtype:"number"},
	template_text_label: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_hover: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_popup: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_sidebar: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_link: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	template_text_url: {type: 'string', maxlength:2000, nullable:true, fieldtype:"varchar2"},
	marker_symbol: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_size: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	marker_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stoke_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	stroke_width: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},
	fill_color: {type: 'string', maxlength:80, nullable:true, fieldtype:"varchar2"},


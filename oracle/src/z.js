//https://stackoverflow.com/questions/32482608/withrelated-binding-is-undefined
var Bookshelf = require('bookshelf');
var knex = require('knex')(require('../config/121test'))
Bookshelf.DB = Bookshelf(knex);

var Zone = require("./models/zone").model;
var Zones = require('./collections/zones').collection;

var newData = require('./zdata1.js')

var proc = setTimeout(knex.destroy, 5000)

newData.forEach(function(e) {
	var i = new Zone(e);
	i.save().then(function(model) {
		console.log('saved', model.get('NAME'))
	})
})
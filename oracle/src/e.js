//https://stackoverflow.com/questions/32482608/withrelated-binding-is-undefined
var Bookshelf = require('bookshelf');
var knex = require('knex')(require('../config/121test'))
Bookshelf.DB = Bookshelf(knex);

var TestEvent = require("./models/testEvent").model;
var TestEvents = require('./collections/testEvents').collection;

var newData = require('./edata.js')

var proc = setTimeout(knex.destroy, 10000)

newData.forEach(function(e) {
	var i = new TestEvent(e);
	i.save().then(function(model) {
		console.log('saved', model.get('OBJ_NAME'))
	}, function() {
		console.log('not saved', model.get('OBJ_NAME'))
	})
})
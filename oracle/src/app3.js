//https://stackoverflow.com/questions/32482608/withrelated-binding-is-undefined
var Bookshelf = require('bookshelf');

var knex = require('knex')({
	client: 'oracledb',
	connection: {
		user          : "gipcoretest",
		password      : "Oz4rsoz4rs#",
		//connectString : '37.59.208.82/oscars',
		//externalAuth  : false,
		host          : "37.59.208.82",
		database      : "oscars"
	},
	fetchAsString: [ 'number', 'clob' ],
	debug: true
})

Bookshelf.DB = Bookshelf(knex);

var Services = require('./collections/services').collection;

var proc = setTimeout(knex.destroy, 10000)

new Services().fetch({
		withRelated: ['rules']
	}).then(function(collection) {
	console.log(collection.toJSON())
	clearTimeout(proc)
	knex.destroy()
});



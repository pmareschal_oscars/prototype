function SendPosition () {
    var self  = this;

    var argv = require('minimist')(process.argv.slice(2));

	var request = require('request');

    self.host = argv.host || "localhost";

    self.port = argv.port || 5554;

    self.url = argv.host || "/send";

    console.log("Poster config: Host: " + self.host + ":" + self.port + " Auth: " + self.auth);

    self.send  =  function (lat,lng) {
        // console.log("Sending position to " + self.host + ":" + self.port + " Auth: " + self.auth);

		var period = 10; // seconds
		var temp = 10 + 6 * Math.sin(Date.now() / (period * 1000)) + Math.random() * 2;

		request.post(
		    'http://'+self.host+':'+self.port+self.url,
		    { json: { name: "test", lat: lat, lon: lng, temp: temp} },
		    function (error, response, body) {
		        if (!error && response.statusCode == 200) {
		            console.log(body)
		        }
		    }
		);
    }
}

var runningAsScript = !module.parent;
if (runningAsScript) {
    var m = new SendPosition();
    m.send(50.639366, 5.445671);
} else {
    module.exports = new SendPosition();
}

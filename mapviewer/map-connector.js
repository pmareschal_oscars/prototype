/**
 * 
 */

var mapviewerUrl = "http://" + document.location.host + "/mapviewer";
var map;
var tileLayer;

var ringLayer100; // ring around airport
var ringLayer200;

var aircraftLayer;  
var aircraftLocalLayer; // aircraft from/to the airport

var petrolTetraLayer; // tetras layers
var fireTetraLayer;
var maintenanceTetraLayer;

var initZoomOutLevel = 8;
var initZoomInLevel = 14;


// list optional modules
var optionalModules = {"snowLayer":false,
                      "snowTetra":false, 
                      "aircraftSize":false,
                      "liveHeatMap":false
                      };



// snow layers (optional module)
var snowLayer;

// new symbols for maintenance tetras (optional module)
var tetraSpecGroupNameArray = ["Fraise", "CJS", "ice"]; // fonts used for tetra group
var tetraSpecGroupArray = ["\uf021", " \uf08b ", "\uf0d0"]; // fonts used for tetra group

// adapt aircraft size with category (optional module)
// change aircraft size on screen based on category


//heatmap live (optional module)
var providerList = [];// list of providers for heatmaps
var heatLayers = []; // list of heatmaps
var heatLifeTime = 1200; // livetime of a heatpoint (in seconds)

var heat = new OM.style.HeatMap(
{                
	styleName:"firstHeatMap",
	colorStops:["#FFFFFF","#00FF00", "#FFC800","#FF0000"],
	spotlightRadius:10.0,
	lengthUnit:"pixel",
	opacity:0.5,
	sampleFactor:2.5
});

function addToHeatMap(feature)
{
	var index = providerList.indexOf(feature.provider); 
	
	//console.log("in add to heatMap : index  : " + index);
	if (index < 0)
	{
		//console.log("want to create layer");
		providerList.push(feature.provider);

		var tmpLayer = new OM.layer.VectorLayer(feature.provider,
				{
				   def:
					{
					type: OM.layer.VectorLayer.TYPE_LOCAL
				   }
				});
		tmpLayer.setRenderingStyle(heat);	
		
		index = (heatLayers.push(tmpLayer))-1;		

		map.addLayer(heatLayers[index], "HeatMap");
		console.log("create heatmap layers : " + feature.provider); 
	}

	var rangePoint = new OM.geometry.Point(feature.x,feature.y,4326);
	var fif = feature.name + "-" + feature.x + "-" +  Date.now();
	//console.log("fif : " + fif);
	var featureHeat = new OM.Feature(fif, rangePoint);
	featureHeat.date = Date.now();
    heatLayers[index].addFeature(featureHeat);
	//heatLayers[0].addFeature(featureHeat); // for global layer
}

function removeLiveHeatmap()
{
	optionalModules["liveHeatMap"]=false;
        //activateLiveHeatMap = false;
	
	for (var i=0 ; i<heatLayers.length; i++)
	{
		//console.log("remove hl " + i);
		map.removeLayer(heatLayers[i]);
	}
	
	heatLayers = [];
	providerList = [];
}


var center = new OM.geometry.Point(5.447038, 50.638274, 4326);
var zoomButtons;

//Unicode Font awesome marker for Tetra group
var tetraGroupArray = {"PETROL_TETRA": "\uf043", "MAINTENANCE_TETRA": "\uf0ad", "FIRE_TETRA": "\uf134"}; // fonts used for tetra group

//Timer to clean 
var featureLifetime = 30000;
var lifeTimer = window.setInterval("cleanNotUpdatedFeatures()", featureLifetime);
var outdatedTime = 50.0;
var aircraftLives = 2;
var aircraftLivesOnParking = 300;

var tetraLives = 10; // live counter

var imageResourcesPath = 'VAADIN/gipmap/images/';

//Remote procedure call for the client to server communication
var rpcProxy;

//be.oscars.gipwebinterface.component.GipMap
window.be_oscars_gipwebinterface_component_GipMap = function (){

    rpcProxy = this.getRpcProxy();

    this.initComponent = function () {

        console.log("Init component");

        //Initialize Dojo (CometD) for pubsub events
        dojo.require("dojo.io.script");
        dojo.require("dojox.cometd");
        dojo.require("dojox.cometd.callbackPollTransport");

        dojo.addOnLoad(function ()
        {
            console.log("on load dojo");
            //  /GIPViewer/cometd
            dojox.cometd.init("/GIPWebInterface/cometd", {
            });
            dojox.cometd.subscribe("/gip/**", onEvent);

            initMap();
        });

    }

}

function getQueryStringParams(p_param) // return a given name paramters value int URL
{
    var sPageURL = window.location.search.substring(1);

    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] == p_param)
        {
            return sParameterName[1];
        }
    }
}

function setValue(p_val,p_defaultVal)
{
	var result = (p_val !== undefined) ? p_val : p_defaultVal;
    return result;
}

function initSnowLayer(p_map)
{
	snowLayer = new OM.layer.VectorLayer(
			"Snow",
			{
				def: 
				{
					   type: OM.layer.VectorLayer.TYPE_PREDEFINED, dataSource: "GipcoreConnection", theme: "LA_SN_THEME", url: mapviewerUrl
				}
			}
		);
		
		snowLayer.setZoomLevelRange(14, 18); //Display tetra only on zoom range 14 - 18
		p_map.addLayer(snowLayer, "Snow");
		snowLayer.setOpacity(0.6);
}

function removeSnowLayer()
{
	map.removeLayer(snowLayer);
        optionalModules["snowLayer"]=false;
	//activateSnowLayer = false;
}

function initMap() {
    
    readUrlParameters(); // read optional modules parameters
    
    
    map = new OM.Map(document.getElementById('map'),
            {
                mapviewerURL: mapviewerUrl  //"http://37.59.208.81:7001/mapviewer"
            });

    tileLayer = new OM.layer.OSMTileLayer("raster");    //Background

    //Rings layer show rings from 100 to 200 km
    ringLayer100 = new OM.layer.VectorLayer("Ring 100 km",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    ringLayer200 = new OM.layer.VectorLayer("Ring 200 km",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    var circleStyleRed = new OM.style.Color(
            {
                fill: "#ff0000", stroke: "#FF0000", strokeThickness: 1, fillOpacity: 0.0, strokeOpacity : 0.8
            });
    var circleStyleBlue = new OM.style.Color(
            {
                fill: "#000000", stroke: "#0000FF", strokeThickness: 1, fillOpacity: 0.0, strokeOpacity : 0.8
            });
    var ring100 = new OM.geometry.Circle(606361.5, 6557556.29, 158000.0, 3857);
    var ring200 = new OM.geometry.Circle(606361.5, 6557556.29, 316000.0, 3857);
    var featureRing100 = new OM.Feature("id100", ring100, {
        renderingStyle: circleStyleRed
    });
    var featureRing200 = new OM.Feature("id200", ring200, {
        renderingStyle: circleStyleBlue
    });
    ringLayer100.addFeature(featureRing100);
    ringLayer200.addFeature(featureRing200);


    aircraftLayer = new OM.layer.VectorLayer("Aircraft (rasp)",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    aircraftLayer.addListener(OM.event.MouseEvent.MOUSE_CLICK, showAircraftInfo);

	    aircraftLocalLayer = new OM.layer.VectorLayer("Aircraft From/To (rasp)",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    aircraftLocalLayer.addListener(OM.event.MouseEvent.MOUSE_CLICK, showAircraftInfo);
	
		
	 

    //Tetra layers
    fireTetraLayer = new OM.layer.VectorLayer("SI",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    fireTetraLayer.setZoomLevelRange(14, 18); //Display tetra only on zoom range 14 - 18
    fireTetraLayer.addListener(OM.event.MouseEvent.MOUSE_CLICK, showTetraInfo);

    maintenanceTetraLayer = new OM.layer.VectorLayer("Maintenance",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    maintenanceTetraLayer.setZoomLevelRange(14, 18); //Display tetra only on zoom range 14 - 18
    maintenanceTetraLayer.addListener(OM.event.MouseEvent.MOUSE_CLICK, showTetraInfo);

    petrolTetraLayer = new OM.layer.VectorLayer("POL",
            {
                def:
                        {
                            type: OM.layer.VectorLayer.TYPE_LOCAL
                        }
            });
    petrolTetraLayer.setZoomLevelRange(14, 18); //Display tetra only on zoom range 14 - 18
    petrolTetraLayer.addListener(OM.event.MouseEvent.MOUSE_CLICK, showTetraInfo);

    map.addLayer(tileLayer);
    map.addLayer(ringLayer200, "Map")
    map.addLayer(ringLayer100, "Map");  
	
	if( true == optionalModules["snowLayer"] ) { initSnowLayer(map); }
	
	map.addLayer(aircraftLocalLayer, "Aircraft");	
    map.addLayer(aircraftLayer, "Aircraft");
	
	map.addLayer(fireTetraLayer, "Tetra");
    map.addLayer(maintenanceTetraLayer, "Tetra")
    map.addLayer(petrolTetraLayer, "Tetra");

    map.init();
    
    setInitZoom(); // read init zoom in url parameters
    
    
    
    
    
    
    

    var layerControl = new OM.control.LayerControl({
        anchorPosition: 1,
        contentStyle: {
            "minWidth": "200", "maxHeight": "300", "font_family": "Tahoma", "font_size": "11px"
        },
        titleStyle: {
            "font_size": "11px", "font_family": "Tahoma"
        }
    });
    map.addMapDecoration(layerControl);

    //\uf0c2
    zoomButtons = new OM.control.MapDecoration("<p style='border-style: solid; border-width: 0px; border-radius: 2px; cursor: pointer;'>  <table border=0>" +
            "<tr><td><button onclick='map.zoomIn();'type='button' style='width: 32px; height: 32px;font-size:28px;color: rgb(135,140,144);font-weight: bold;line-height:0;'>+</button> </td></tr>" +
            "<tr><td><button onclick='map.zoomOut();'type='button' style='width: 32px; height: 32px;font-size:32px;color: rgb(135,140,144);font-weight: bold;line-height:0;'>-</button> </td></tr>" +
            "<tr><td><button onclick='map.setMapCenterAndZoomLevel(center, initZoomInLevel, true);'type='button' style='width: 32px; height: 32px;'><p style=\"font-family:FontAwesome;color: rgb(135,140,144);opacity:1.0;line-height:0;\">\uf018</p></button> </td></tr>" +
            "<tr><td><button onclick='map.setMapCenterAndZoomLevel(center, initZoomOutLevel , true);'type='button' style='width: 32px; height: 32px;'><p style=\"font-family:FontAwesome;color: rgb(135,140,144);opacity:1.0;line-height:0;\">\uf0ac</p> </button></td></tr></table></p>",
            {
                contentStyle: {
                    "border-width": "0px",
                    "font-size": "16px",
                    "color": "#000000",
                    "opacity": "0.75",
                    "background-color": "none"
                }

            });
    zoomButtons.setAnchorPosition(3);
    zoomButtons.setDraggable(false);
    map.addMapDecoration(zoomButtons);

    var scaleBar = new OM.control.ScaleBar({anchorPosition: 4});
    map.addMapDecoration(scaleBar);

	/*
    var copyright = new OM.control.MapDecoration(
            "<img src=\"http://www.andenne-airport.com/sites/all/themes/custom/expansion/dist/img/logo.png\" height='80' width='240' style='background: rgba(0,0,0,0.6); padding : 5px'>",
            {
                anchorPosition: 6,
                contentStyle: {
                    "color": "#d3d3d3"
                }
            });
    copyright.setDraggable(false);
    map.addMapDecoration(copyright);
*/
}

function setInitZoom()
{
	zoomInit = getQueryStringParams('zoom'); // search "zoom" in url params
        
        //console.log (zoomInit);
	
        if(zoomInit != undefined)
	{
		if(zoomInit == "true")
		{
			map.setMapCenterAndZoomLevel(center, initZoomInLevel, true);
		}
		if(zoomInit == "false")
		{
			map.setMapCenterAndZoomLevel(center, initZoomOutLevel, true);
		}
		else if (/^\d+$/.test(zoomInit))
		{
			map.setMapCenterAndZoomLevel(center, parseInt(zoomInit), true);
		}
	}
        else
        {
            map.setMapCenterAndZoomLevel(center, initZoomOutLevel, true);
        }
}

function readUrlParameters()
{

    for(var i in optionalModules)
    {
        var activate = getQueryStringParams(i);
        if(activate != undefined)
            {
                if(activate == "true")
                {
                    console.log("module : " + i + " activate")
                     optionalModules[i]=true; 
                }    
            }        
    }
}
    
    
function onEvent(pubsubEvent) 
{
	var ch = pubsubEvent.channel;
    if (ch == "/gip/geo")
	{	
        showEventDevicesPositions(pubsubEvent.data);
	}
}

function showEventDevicesPositions(event) 
{
    //console.log("Device position event received");
    event = JSON.parse(event.data);

    switch (event.properties.attributes.device_type)
    {
        case 'AircraftPositionEvent':
            //seen_pos : age in seconds of last received position, seen : age in seconds of last message.
            if (event.properties.device.status == 'ACTIVE')
            {
                var feat = null;
                var name = event.properties.attributes.name;

                if (aircraftLocalLayer.getFeature(name)) // know feature
                {
                    feat = aircraftLocalLayer.getFeature(name);
                }
				else if (aircraftLayer.getFeature(name)) // know feature
                {
                    feat = aircraftLayer.getFeature(name);
                }

                if (event.properties.attributes.payload.seen_pos > outdatedTime || event.properties.attributes.payload.seen > outdatedTime)
                {
					//oudated info, don't care about it 
					
                    //console.log("CC candidate oudated : " + name);
                    /*if (feat != null) 
					{
                       // console.log("CC remove outdated aircraft " + name + " : " + event.properties.attributes.payload.seen_pos);
                       //aircraftLayer.removeFeature(feat);
                    }*/
                } 
				else if (isNewerInfo(feat, event))
                {
					var targetLayer = aircraftLayer;
						 
				    if (event.properties.attributes.direction == "D" ||event.properties.attributes.direction == "A") // its a local aircraft
					{
						targetLayer = aircraftLocalLayer; 
					}
				
                    var fixedHeading = (event.properties.attributes.heading + 315) % 360; // fix heading with fontAwesome

                    var aircraftPoint = new OM.geometry.Point(
                            event.properties.attributes.x,
                            event.properties.attributes.y,
                            4326,
                            Math.cos(fixedHeading * Math.PI / 180),
                            -Math.sin(fixedHeading * Math.PI / 180)
                            );
							
                    addFeatureToLayer(event, aircraftPoint, targetLayer);
                }
				else // we have already a newest info
                {
                    //console.log("rejected : " + name);
                }
            }
            break;

        case 'TetraPositionEvent':
            var tetraPoint = new OM.geometry.Point(
                    event.properties.attributes.x,
                    event.properties.attributes.y,
                    4326
                    );
            switch (event.properties.device.devicesGroup.name) {
                case 'PETROL_TETRA' :
                    addFeatureToLayer(event, tetraPoint, petrolTetraLayer);
                    break;
                case 'FIRE_TETRA' :
                    addFeatureToLayer(event, tetraPoint, fireTetraLayer);
                    break;
                case 'MAINTENANCE_TETRA' :
                    addFeatureToLayer(event, tetraPoint, maintenanceTetraLayer);
                    break;
                default:
                    console.log("Unknown device group");
            }
            break;
        default :
            console.log("Unknown envent type");
    }
}

function addFeatureToLayer(event, point, layer)
{
	
	//console.log(event.properties.attributes.heading);
    var feature;
    var oriented = event.properties.attributes.heading ? true : false;
    var newFeat = false;

    var name = event.properties.attributes.name;

    if (!layer.getFeature(name)) //not in the layer -> its a new feature 
	{ 
        newFeat = true;
        feature = new OM.Feature(name, point);

		feature.aliveCounter = aircraftLives; // very important, set live counter for the new feature
		
        //set static label properties of the feature
        switch (event.properties.attributes.device_type)
        {
            case 'AircraftPositionEvent':
                feature.setLabel(event.properties.attributes.payload.flight.trim());
                break;
            case 'TetraPositionEvent':
                feature.setLabel(name);
                break;
        }

    } 
	else //Update existing feature
	{ 
        feature = layer.getFeature(name);
        feature.setGeometry(point);
    }

    //Enrich feature
    feature.name = name;
    feature.x = event.properties.attributes.x;
    feature.y = event.properties.attributes.y;
	feature.provider = event.properties.attributes.provider;
	//console.log(feature.provider);
    //feature.positiondate = event.properties.attributes.positiondate;

    //Change dynamic properties of the feature
    switch (event.properties.attributes.device_type)
    {
        case 'AircraftPositionEvent':
            //console.log("TTAAATTry to ape");
		
			if(feature.aliveCounter < 2 || isNaN(feature.aliveCounter))
			{
				feature.aliveCounter = 2;
				//console.log (feature.name + " NAN !!!" +  feature.aliveCounter);
			}
			else
			{
				feature.aliveCounter = Math.max(feature.aliveCounter, aircraftLives); // new DDU
				 //console.log (feature.name + " refresh lives : " +  feature.aliveCounter);
			}
			
            feature.altitude = setValue(event.properties.attributes.altitude, null);
            feature.category = setValue(event.properties.attributes.payload.category,"A0");
           	feature.direction = setValue(event.properties.attributes.direction,null);
			feature.flight = setValue(event.properties.attributes.payload.flight,"UNKNOW");
			feature.speed = setValue(event.properties.attributes.speed,null);
           			
            feature.positiondate = getRealPositionDate(event.properties.attributes.positiondate, event.properties.attributes.payload.seen_pos); 
		

            feature.setMarkerText('\uf072');
			
			if ("A" == feature.direction)
			{
				if(newFeat)
				{
					console.log ("New coming aircraft detected : " +  feature.flight);
				}	
				
				altitudeCheck(feature,newFeat);
				speedCheck(feature,newFeat);
			}
			
			feature.setRenderingStyle(getAircraftMarker(feature));

			break;
        case 'TetraPositionEvent':
		
			//console.log("Tetra move !!!");
		
            feature.aliveCounter = tetraLives;

            feature.positiondate = event.properties.attributes.positiondate;

            feature.tetraGroup = event.properties.device.devicesGroup.displayName;
            //don't use the 'group' of the mapviewer feature because it's reserved
            feature.setMarkerText(tetraGroupArray[event.properties.device.devicesGroup.name]);
			
			if( true == optionalModules["snowTetra"])
			{
				if(feature.tetraGroup == "Maintenance")
				{
							
					for (var i=0 ; i<tetraSpecGroupArray.length; i++)
					{
						//console.log ( feature.name + "  match111 : " + tetraSpecGroupNameArray[i]);
						if (feature.name.includes(tetraSpecGroupNameArray[i]))
						{
							feature.setMarkerText(tetraSpecGroupArray[i]);
							i = tetraSpecGroupArray.length;
						//	console.log ("match : " + feature.name);
						}
					}
				}
			}
						
            feature.setRenderingStyle(getTetraMarker());

            break;
        default :
            console.log("Unknown envent type");
    }

	//console.log("herehere");
	if (true == optionalModules["liveHeatMap"])
	{
		//console.log("in if");
		addToHeatMap(feature);
	}
	
	
    if (newFeat)
	{
        layer.addFeature(feature);
    }
}

function getGhostMarker() // nearly removed or not moving 
{
	var takeOffText = new OM.style.Text(
            {
                fontSize: 22, fontWeight: OM.Text.FONTWEIGHT_NORMAL, fill: "#808080",fontFamily: 'FontAwesome'
            });

			var circleStyleCustom = new OM.style.Marker(
            {
                textStyle: takeOffText,
                vectorDef: [{shape: {type: "circle", cx: 0, cy: 0, width: 25, height: 25}, style: {fill:"#FF0000", stroke:"#808080", strokeThickness: 2, fillOpacity: 0.0, strokeOpacity: 0.5}}]
            });
			
	return circleStyleCustom;
}

function getAircraftMarker(p_feature) 
{
	var fontSize = 22;
	
	var symbolColor = '#000000';
	
	var borderColor = '#000000';
	var borderOpa = 0.0;
	
    var fillColor = '#000000';
	var fillOpa = 0.0;
    
	
	if (true == optionalModules["aircraftSize"])
	{
		//console.log(p_feature.category.charAt(1));
		fontSize = fontSize + 2 * (Number(p_feature.category.charAt(1)));
		//console.log(fontSize);
		
		if( p_feature.category.charAt(1) == "0" )	
		{
			symbolColor = '#001AA0';
		}
	}
	if (p_feature.onGround && p_feature.stopped)
	{
		symbolColor = '#808080';
	}
	
	if (p_feature.direction == "A" || p_feature.direction == "D") //arrival color
	{
		//symbolColor = "#E10004"; //"#E37222";" rose #EB13EB";
		borderColor = "#E37222";// "#2A6EBB";
		borderOpa = 1.0;
		var fillColor = '#E37222';
		var fillOpa = 0.4;
	}
	//TODO wait modif from GIP
	/*
	else if (p_feature.direction == "D") // departure color 
	{
		//symbolColor = "#0026FF"; //"#2A6EBB";
		borderColor = "#2A6EBB"; //"#E37222";
		borderOpa = 1.0;
		var fillColor = '#2A6EBB';
		var fillOpa = 0.4;
	}
	*/
	
	
	var circleSize = fontSize + 2;
	
    var aircraftText = new OM.style.Text(
            {
                fontSize: fontSize, fontWeight: OM.Text.FONTWEIGHT_NORMAL, fill: symbolColor, fontFamily: 'FontAwesome'
            });

    var marker = new OM.style.Marker(
            {
                textStyle: aircraftText,
                vectorDef: [{shape: {type: "circle", cx: 0, cy: 0, width: circleSize, height: circleSize}, style: {fill: fillColor, fillOpacity: fillOpa, stroke:borderColor, strokeThickness: 2,strokeOpacity: borderOpa}}]
            });

    return marker;
}

function getTetraMarker()
{
    var textColor = '#000000';
    var fillColor = '#000000';
    var borderColor = '#000000';

    var tetraText = new OM.style.Text(
            {
                fontSize: 20, fontWeight: OM.Text.FONTWEIGHT_NORMAL, fill: textColor, fontFamily: 'FontAwesome'
            });

    var marker = new OM.style.Marker(
            {
                textStyle: tetraText,
                vectorDef: [{shape: {type: "circle", cx: 0, cy: 0, width: 25, height: 25}, style: {fill: fillColor, stroke: borderColor, strokeThickness: 2, fillOpacity: 0.2}}]
            });

    return marker;
}


function includes(container, value) // fix for IE bug 
{
	var res = false;
	var pos = container.indexOf(value);
	if (pos >= 0) 
	{
		res = true;
	}
	return res;
}



function cleanNotUpdatedFeatures()
{
    //The aircrafts/tetras begin with aliveCounter = 1 and 5 and is decremented at each clean. When aliveCounter < -1 then the aircraft has to be removed.

    //console.log("cleanNotUpdatedFeatures call !");
    map.getFeatureLayers().forEach(function (layerItem)
    {
        //console.log(layerItem.getName());
		if(includes(layerItem.getName(),"Provider") == false)
		{
			//console.log("Clean : " + layerItem.getName());
			layerItem.getAllFeatures().forEach(function (featureItem)
			{
				featureItem.aliveCounter -= 1;
					
				//console.log(layerItem.getName() + " -> " + featureItem.flight + " -> counter : " + featureItem.aliveCounter);
				if (featureItem.aliveCounter == 0) // nearly removed aircraft
				{
					//console.log(layerItem.getName() + " " + featureItem.flight + " : " + featureItem.aliveCounter + " -> nearlyRemoved feature");
					featureItem.setRenderingStyle(getGhostMarker());
				}
				else if (featureItem.aliveCounter < -1) // remove aircraft
				{
					//console.log(layerItem.getName() + "-> Remove a feature" + " " + featureItem.flight );
					//addToRangeLayer(featureItem,true);
					layerItem.removeFeature(featureItem);
				}
				else if (featureItem.aliveCounter > 50) 
				{
				   // console.log(featureItem.flight + "-> alive counter : " + " " + featureItem.aliveCounter );
				}
				
			});
		}
		else
		{
			//console.log("initDate");

			var d = new Date();
			
			//console.log("before set seconds");
			
			var n = d.setSeconds(d.getSeconds() - heatLifeTime); 

	
			//console.log("before loop");
			
			layerItem.getAllFeatures().forEach(function (featureItem)
			{
				//console.log("candidate feat : " + featureItem.date + " < " + n);
				
				if (featureItem.date < n )
				{
				//	console.log(featureItem.date);
			
					layerItem.removeFeature(featureItem);
				}
			});
		}
			
		layerItem.redraw(); 
	});
}

function isNewerInfo(feature, event) // incoming event is newer than current position ?
{
    var result = true;
    //console.log("check feat not null");
    if (feature != null)
    {
        //console.log("feat not null");

        switch (event.properties.attributes.device_type)
        {
            case 'AircraftPositionEvent':
                var eventRealDate = getRealPositionDate(event.properties.attributes.positiondate, event.properties.attributes.payload.seen_pos);

                if (eventRealDate <= feature.positiondate)
                {
                    result = false;
                    //console.log(new Date(eventRealDate) + " <= " + new Date(feature.positiondate) + " seen_pos : " + event.properties.attributes.payload.seen_pos);
                } else
                {
                    //console.log(new Date(eventRealDate) + " > " + new Date(feature.positiondate) + " seen_pos : " + event.properties.attributes.payload.seen_pos);
                }
                break;
        }
    } // if feat == null, its a new feature
    return result;
}


function getRealPositionDate(p_time, p_seenPos) // (positionDate - seenPos) = real date
{
    var result = new Date(p_time);
    result = result.setMilliseconds(result.getMilliseconds() - (p_seenPos * 1000));
    return result;
}

function showAircraftInfo(event) {
    var aircraft = new Object();
    aircraft.name = event.feature.id;
    aircraft.flight = event.feature.flight;
    aircraft.longitude = event.feature.x;
    aircraft.latitude = event.feature.y;
    aircraft.altitude = event.feature.altitude;
    aircraft.speed = event.feature.speed;
    rpcProxy.onAircraftClick(aircraft);
}

function showTetraInfo(event) {
    var tetra = new Object();
    tetra.name = event.feature.id;
    tetra.group = event.feature.tetraGroup;
    tetra.longitude = event.feature.x;
    tetra.latitude = event.feature.y;
    console.log(event.feature.tetraGroup);
    rpcProxy.onTetraClick(tetra);
}



function speedCheck(p_feature,p_newFeat)
{
	if(p_newFeat) // new feature
	{
		if (p_feature.speed > 0.0)
		{
			p_feature.stopped = false;
		}
		else if (speed == 0)
		{
			p_feature.stopped = true;
		}
	}
	else
	{
		if (p_feature.speed > 5.0 && p_feature.stopped == true) // > 5.0 to avoid gps starBug
		{
			 console.log(p_feature.name + " " + p_feature.flight +" Speed : start moving " + new Date().toUTCString()+ " - " + p_feature.x + " " + p_feature.y);
			p_feature.stopped = false;
			//reset alive counter
			
		}
		else if (p_feature.speed == 0.0 && p_feature.stopped == false && p_feature.altitude == 0.0)
		{
			console.log(p_feature.name + " " + p_feature.flight +" Speed : stop moving " + new Date().toUTCString()+ " - " + p_feature.x + " " + p_feature.y);
			p_feature.aliveCounter = aircraftLivesOnParking; // stay alive on parking
			p_feature.stopped = true;
		}
	}
}


function altitudeCheck(p_feature, p_newFeat)
{
	if(p_newFeat) // new feature
	{
		if (p_feature.altitude > 0.0)
		{
			p_feature.onGround = false;
		}
		else
		{
			p_feature.onGround = true;
		}
	}
	else
	{
		if (p_feature.altitude > 50.0 && p_feature.onGround == true)
		{
			console.log(p_feature.name + " " + p_feature.flight + " Altitude : takeoff " + new Date().toUTCString()+ " - " + p_feature.x + " " + p_feature.y);
			p_feature.onGround = false;
		}

		else if (p_feature.altitude == 0.0 && p_feature.onGround == false)
		{
			console.log(p_feature.name + " " + p_feature.flight +" Altitude : landing " + new Date().toUTCString()+ " - " + p_feature.x + " " + p_feature.y);
			p_feature.onGround = true;
		}
	}
}

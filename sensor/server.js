const express = require('express')
const bodyParser= require('body-parser')
const app = express()
const config = require('./config')

var sensor = require('./lib/sensor')

var http = require('http');
var server = http.Server(app)
var io = require('socket.io')(server)

var cjson = require('cjson');

app.use('/bower', express.static(__dirname + '/bower_components'))

app.use('/scripts', express.static(__dirname + '/node_modules'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/fr24.html')
})


function fr24feed() {
    var data = '';

	var request = http.request(config.fr24feed, function (res) {

	    res.on('data', function (chunk) {
	        data += chunk;
	    });

	    res.on('end', function () {
			var planes = JSON.parse(data)
			
			planes.forEach(function(plane) {
				if(plane.validposition == 1) {
					var p = {
					  "type": "Feature",
					  "geometry": {
					    "type": "Point",
					    "coordinates": [plane.lon, plane.lat, plane.altitude]
					  },
					  "properties": plane
					}
					io.sockets.emit('plane', p)
				}
			});
	    });

		request.on('error', function (e) {
		    console.log(e.message);
		});

	});
	request.end();
}

io.on('connection', function(socket) {
	
	setInterval(fr24feed, 10000);

})

server.listen(config.web.port, () => {
    console.log('listening on '+config.web.port)
})

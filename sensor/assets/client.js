// require leaflet.js
var L = require('leaflet'),
	DataTransform = require('node-json-transform').DataTransform,
	Chart = require('chart.js'),
	moment = require("moment");

var geoJson = require("../data/sample-geojson2.json");

function sortByKey(array, key) {
	return array.sort(function(a, b) {
	    var x = a[key]; var y = b[key];
	    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}


// 1. Prepare Map. Display layer.
var map = L.map('map').setView([40.73, -74.21], 13);

var geojsonMarkerOptions = {
    radius: 2,
    fillColor: "#ff7800",
    color: "#ff7800",
    weight: 1,
    opacity: 0.5,
    fillOpacity: 0.5
};

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a>'
}).addTo(map);

L.geoJson(geoJson, {
	pointToLayer: function (feature, latlng) {
			var circ = L.circleMarker(latlng, geojsonMarkerOptions);
			circ.on('mouseover', function(){console.log(feature.properties.OBJECTID); circ.bindPopup('hi').openPopup();});
	        return circ;
	}
}).addTo(map);

// 2. Prepare ChartJS data
var map = {
    list : 'features',
    item: {
        x: "properties.DateTime_",
		y: "properties.speed",
		z: "properties.OBJECTID"
    },
    operate: [{
		run: function(val) { return parseFloat(val.replace(" mph","")); }, on: "y"
    },{
	    run: function(val) { return moment(val, "YYYYMMDDHHmmss").format(); }, on: "x"
    }]
};

var dataTransform = DataTransform(geoJson, map);
var result2 = dataTransform.transform();
var result = sortByKey(result2, 'x');
// console.log(result);
		
// 3. Chart it
var canvas = document.getElementById("chart").getContext("2d");

var myChart = new Chart(canvas, {
    type: 'line',
    data: {
        datasets: [{
            label: 'Speed',
            data: result
        }]
    },
    options: {
        scales: {
            xAxes: [{
                type: 'time',
				time: {
                    unit: 'hour'
                },
                position: 'bottom'
            }]
        },
		responsive: true,
		maintainAspectRatio: false
    }
});

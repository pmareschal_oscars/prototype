const express = require('express')
const MongoClient = require('mongodb').MongoClient
const bodyParser= require('body-parser')
const app = express()

const config = require('./config/sensor.js')
var sensor = require('./lib/sensor')

var server = require('http').Server(app)
var io = require('socket.io')(server)

var db

app.use('/scripts', express.static(__dirname + '/node_modules'))

app.use('/bower', express.static(__dirname + '/bower_components'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/live.html')
})

app.use(bodyParser.urlencoded({extended: true}))

var gloCounter = 0;

function saveAndEmit(doc, message) {
	var p = sensor.makeGeoJsonSensorData(doc)
	if(p) {
	    db.collection(config.mongodb.collection).save(p, (err, result) => {
	        if (err)
				return console.log(err)
			console.log('.')
	    })
	    if(io.sockets) {
			io.sockets.emit(message, p)
			io.sockets.emit('dataSet', [gloCounter++, doc.accelerometerAccelerationX] )
			console.log('+')
		}
	}
}

app.post('/sensor-log', (req, res) => {
	saveAndEmit(req.body, 'newdata')	
})

io.on('connection', function(socket) {
	console.log('Connected.')
})

MongoClient.connect(config.mongodb.url, (err, database) => {
    if (err) return console.log(err)
    db = database
    server.listen(config.web.port, () => {
        console.log('listening on '+config.web.port)
    })
})

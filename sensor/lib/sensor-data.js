/* Note: CSV file has one more field
loggingTime
loggingSample		<<<<<<<<
identifierForVendor
*/
var sensorDataModel = {
  loggingTime: Date,
  identifierForVendor: String,
  deviceID: String,
  locationTimestamp_since1970: String,
  locationLatitude: String,
  locationLongitude: String,
  locationAltitude: String,
  locationSpeed: String,
  locationCourse: String,
  locationVerticalAccuracy: String,
  locationHorizontalAccuracy: String,
  locationFloor: String,
  locationHeadingTimestamp_since1970: String,
  locationHeadingX: String,
  locationHeadingY: String,
  locationHeadingZ: String,
  locationTrueHeading: String,
  locationMagneticHeading: String,
  locationHeadingAccuracy: String,
  accelerometerTimestamp_sinceReboot: String,
  accelerometerAccelerationX: String,
  accelerometerAccelerationY: String,
  accelerometerAccelerationZ: String,
  gyroTimestamp_sinceReboot: String,
  gyroRotationX: String,
  gyroRotationY: String,
  gyroRotationZ: String,
  motionTimestamp_sinceReboot: String,
  motionYaw: String,
  motionRoll: String,
  motionPitch: String,
  motionRotationRateX: String,
  motionRotationRateY: String,
  motionRotationRateZ: String,
  motionUserAccelerationX: String,
  motionUserAccelerationY: String,
  motionUserAccelerationZ: String,
  motionAttitudeReferenceFrame: String,
  motionQuaternionX: String,
  motionQuaternionY: String,
  motionQuaternionZ: String,
  motionQuaternionW: String,
  motionGravityX: String,
  motionGravityY: String,
  motionGravityZ: String,
  motionMagneticFieldX: String,
  motionMagneticFieldY: String,
  motionMagneticFieldZ: String,
  motionMagneticFieldCalibrationAccuracy: String,
  activityTimestamp_sinceReboot: String,
  activity: String,
  activityActivityConfidence: String,
  activityActivityStartDate: Date,
  pedometerStartDate: String,
  pedometerNumberOfSteps: String,
  pedometerDistance: String,
  pedometerFloorsAscended: String,
  pedometerFloorsDescended: String,
  pedometerEndDate: String,
  IP_en0: String,
  IP_pdp_ip0: String,
  deviceOrientation: String,
  batteryLevel: String,
  batteryState: String,
  state: String,
  _id: 57de6b9ef9ec3ea76cf6ff01
}
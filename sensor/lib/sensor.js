(function(Sensor) {

Sensor.version = '0.0.1';

// Allow user to specify default parameters
Sensor.defaults = {};

function applyDefaults(params, defaults) {
  var settings = params || {};

  for(var setting in defaults) {
    if(defaults.hasOwnProperty(setting) && !settings[setting]) {
      settings[setting] = defaults[setting];
    }
  }

  return settings;
}

/**
 *	Alternative: Use https://github.com/caseypt/geojson.js.
 */
Sensor.makeGeoJsonSensorData = function (doc) {
	if(Sensor.isSensorData(doc)) {
		return {
		  "type": "Feature",
		  "geometry": {
		    "type": "Point",
		    "coordinates": [doc['locationLongitude'], doc['locationLatitude'], doc['locationAltitude']]
		  },
		  "properties": doc
		}
	}
	return null
}

Sensor.getFeatureId = function (doc) { // this allow to simulate several devices with state added
	return doc.properties.deviceID + doc.properties.state
}

Sensor.isSensorData = function (o) {
	// return o instanceof SensorData
	return typeof o['locationLatitude'] != "undefined"
}

Sensor.isLoggedSensorData = function (o) {
	// return o instanceof SensorData
	return typeof o['loggingSample'] != "undefined"
}


}(typeof module == 'object' ? module.exports : window.Sensor = {}));
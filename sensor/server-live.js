/**
 * Live version: Waits for post on senso-log and forward message to browser for display.
 *
 */
const config = require('./config')

const express = require('express')
const MongoClient = require('mongodb').MongoClient
const bodyParser= require('body-parser')
const app = express()

var destination = require('turf-destination')

var sensor = require('./lib/sensor')

var server = require('http').Server(app)
var io = require('socket.io')(server)

var fs = require('fs')
var csv = require('fast-csv')

var db

var mongoose = require('mongoose')
mongoose.connect('mongodb://New-iMac.local:27017/iosdata')

var GeoJSONReq = require('mongoose-geojson-schema')
var geojsonSchema = new mongoose.Schema({
	any: mongoose.Schema.Types.GeoJSON,
	point: mongoose.Schema.Types.Point,
	multipoint: mongoose.Schema.Types.MultiPoint,
	linestring: mongoose.Schema.Types.LineString,
	multilinestring: mongoose.Schema.Types.MultiLineString,
	polygon: mongoose.Schema.Types.Polygon,
	multipolygon: mongoose.Schema.Types.MultiPolygon,
	geometry: mongoose.Schema.Types.Geometry,
	geometrycollection: mongoose.Schema.Types.GeometryCollection,
	feature: mongoose.Schema.Types.Feature,
	featurecollection: mongoose.Schema.Types.FeatureCollection
})
var GeoJSON = mongoose.model('GeoJSON', geojsonSchema)

/* Note: CSV file has one more field
loggingTime
loggingSample		<<<<<<<<
identifierForVendor
*/
var sensorDataSchema = mongoose.Schema({
    loggingTime: Date,
    identifierForVendor: String,
    deviceID: String,
    locationTimestamp_since1970: String,
    locationLatitude: String,
    locationLongitude: String,
    locationAltitude: String,
    locationSpeed: String,
    locationCourse: String,
    locationVerticalAccuracy: String,
    locationHorizontalAccuracy: String,
    locationFloor: String,
    locationHeadingTimestamp_since1970: String,
    locationHeadingX: String,
    locationHeadingY: String,
    locationHeadingZ: String,
    locationTrueHeading: String,
    locationMagneticHeading: String,
    locationHeadingAccuracy: String,
    accelerometerTimestamp_sinceReboot: String,
    accelerometerAccelerationX: String,
    accelerometerAccelerationY: String,
    accelerometerAccelerationZ: String,
    gyroTimestamp_sinceReboot: String,
    gyroRotationX: String,
    gyroRotationY: String,
    gyroRotationZ: String,
    motionTimestamp_sinceReboot: String,
    motionYaw: String,
    motionRoll: String,
    motionPitch: String,
    motionRotationRateX: String,
    motionRotationRateY: String,
    motionRotationRateZ: String,
    motionUserAccelerationX: String,
    motionUserAccelerationY: String,
    motionUserAccelerationZ: String,
    motionAttitudeReferenceFrame: String,
    motionQuaternionX: String,
    motionQuaternionY: String,
    motionQuaternionZ: String,
    motionQuaternionW: String,
    motionGravityX: String,
    motionGravityY: String,
    motionGravityZ: String,
    motionMagneticFieldX: String,
    motionMagneticFieldY: String,
    motionMagneticFieldZ: String,
    motionMagneticFieldCalibrationAccuracy: String,
    activityTimestamp_sinceReboot: String,
    activity: String,
    activityActivityConfidence: String,
    activityActivityStartDate: Date,
    pedometerStartDate: String,
    pedometerNumberOfSteps: String,
    pedometerDistance: String,
    pedometerFloorsAscended: String,
    pedometerFloorsDescended: String,
    pedometerEndDate: String,
    IP_en0: String,
    IP_pdp_ip0: String,
    deviceOrientation: String,
    batteryLevel: String,
    batteryState: String,
    state: String
})
var SensorData = mongoose.model('SensorData', sensorDataSchema)

app.use('/bower', express.static(__dirname + '/bower_components'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/live.html')
})

app.use(bodyParser.urlencoded({extended: true}))

var lastPosition = null

function move(doc, distance) {
	if(!lastPosition) lastPosition = doc	
	heading = Math.round(doc.properties.locationMagneticHeading)
	lastPosition = destination(lastPosition.geometry, distance * Math.random(), heading <= 180 ? heading : heading - 360, 'kilometers')
	doc.geometry.coordinates = lastPosition.geometry.coordinates
}

app.post('/sensor-log', (req, res) => {
	var doc = req.body
	if(sensor.isSensorData(doc)) {
	    db.collection('iosdata').save(doc, (err, result) => {
	        if (err) return console.log(err)

	        // console.log('saved to database')
	    })
	    if(io.sockets) {
			var p = sensor.makeGeoJsonSensorData(doc)
			if(p) {
				move(p, 0.1)
				io.sockets.emit('newdata', p)
			}
		}
	}
})

app.get('/sensor-plot', (req, res) => {
    db.collection("iosdata").find({}).toArray(function(err, docs) {
        if ( err ) throw err
        var posArray = []

        for (index in docs) {
            var doc = docs[index]
			var p = sensor.makeGeoJsonSensorData(doc)
            if(p) {
				posArray.push(p)
            }
        }
		res.send(posArray) 
    })
   console.log('data loaded')
})

io.on('connection', function(socket) {
	console.log('Connected to client')
	lastPosition = null
})


MongoClient.connect('mongodb://New-iMac.local:27017/iosdata', (err, database) => {
    if (err) return console.log(err)
    db = database
    server.listen(config.web.port, () => {
        console.log('listening on '+config.web.port)
    })
})
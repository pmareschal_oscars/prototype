const express = require('express')
const MongoClient = require('mongodb').MongoClient
const bodyParser= require('body-parser')
const app = express()
const config = require('./config/sensor')

var sensor = require('./lib/sensor')

var server = require('http').Server(app)
var io = require('socket.io')(server)

var fs = require('fs')
var csv = require('fast-csv')

var db

var gloCounter = 0;

var mongoose = require('mongoose')
mongoose.connect('mongodb://New-iMac.local:27017/iosdata')

var GeoJSONReq = require('mongoose-geojson-schema')
var geojsonSchema = new mongoose.Schema({
	any: mongoose.Schema.Types.GeoJSON,
	point: mongoose.Schema.Types.Point,
	multipoint: mongoose.Schema.Types.MultiPoint,
	linestring: mongoose.Schema.Types.LineString,
	multilinestring: mongoose.Schema.Types.MultiLineString,
	polygon: mongoose.Schema.Types.Polygon,
	multipolygon: mongoose.Schema.Types.MultiPolygon,
	geometry: mongoose.Schema.Types.Geometry,
	geometrycollection: mongoose.Schema.Types.GeometryCollection,
	feature: mongoose.Schema.Types.Feature,
	featurecollection: mongoose.Schema.Types.FeatureCollection
})
var GeoJSON = mongoose.model('GeoJSON', geojsonSchema)

/* Note: CSV file has one more field
loggingTime
loggingSample		<<<<<<<<
identifierForVendor
*/
var sensorDataSchema = mongoose.Schema({
    loggingTime: Date,
    identifierForVendor: String,
    deviceID: String,
    locationTimestamp_since1970: String,
    locationLatitude: String,
    locationLongitude: String,
    locationAltitude: String,
    locationSpeed: String,
    locationCourse: String,
    locationVerticalAccuracy: String,
    locationHorizontalAccuracy: String,
    locationFloor: String,
    locationHeadingTimestamp_since1970: String,
    locationHeadingX: String,
    locationHeadingY: String,
    locationHeadingZ: String,
    locationTrueHeading: String,
    locationMagneticHeading: String,
    locationHeadingAccuracy: String,
    accelerometerTimestamp_sinceReboot: String,
    accelerometerAccelerationX: String,
    accelerometerAccelerationY: String,
    accelerometerAccelerationZ: String,
    gyroTimestamp_sinceReboot: String,
    gyroRotationX: String,
    gyroRotationY: String,
    gyroRotationZ: String,
    motionTimestamp_sinceReboot: String,
    motionYaw: String,
    motionRoll: String,
    motionPitch: String,
    motionRotationRateX: String,
    motionRotationRateY: String,
    motionRotationRateZ: String,
    motionUserAccelerationX: String,
    motionUserAccelerationY: String,
    motionUserAccelerationZ: String,
    motionAttitudeReferenceFrame: String,
    motionQuaternionX: String,
    motionQuaternionY: String,
    motionQuaternionZ: String,
    motionQuaternionW: String,
    motionGravityX: String,
    motionGravityY: String,
    motionGravityZ: String,
    motionMagneticFieldX: String,
    motionMagneticFieldY: String,
    motionMagneticFieldZ: String,
    motionMagneticFieldCalibrationAccuracy: String,
    activityTimestamp_sinceReboot: String,
    activity: String,
    activityActivityConfidence: String,
    activityActivityStartDate: Date,
    pedometerStartDate: String,
    pedometerNumberOfSteps: String,
    pedometerDistance: String,
    pedometerFloorsAscended: String,
    pedometerFloorsDescended: String,
    pedometerEndDate: String,
    IP_en0: String,
    IP_pdp_ip0: String,
    deviceOrientation: String,
    batteryLevel: String,
    batteryState: String,
    state: String
})
var SensorData = mongoose.model('SensorData', sensorDataSchema)

app.use('/scripts', express.static(__dirname + '/node_modules'))

app.use('/bower', express.static(__dirname + '/bower_components'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/index2.html')
})

app.use(bodyParser.urlencoded({extended: true}))

function saveAndEmit(doc, message) {
	if(sensor.isSensorData(doc)) {
	    db.collection('iosdata').save(doc, (err, result) => {
	        if (err) return console.log(err)

	        // console.log('saved to database')
	    })
	    if(io.sockets) {
			var p = sensor.makeGeoJsonSensorData(doc)
			if(p) {
				io.sockets.emit(message, p)
				io.sockets.emit('dataSet', [gloCounter++, doc.accelerometerAccelerationX] )
			}
		}
	}
}

app.post('/sensor-log', (req, res) => {
	
	saveAndEmit(req.body, 'newdata')	

})


app.get('/sensor-plot', (req, res) => {
    db.collection("iosdata").find({}).toArray(function(err, docs) {
        if ( err ) throw err
        var posArray = []

        for (index in docs) {
            var doc = docs[index]
			var p = sensor.makeGeoJsonSensorData(doc)
            if(p) {
				posArray.push(p)
            }
        }
		res.send(posArray) 
    })
   console.log('data loaded')
})

io.on('connection', function(socket) {

	// var fields = Object.keys(sensorDataSchema.paths)
	var fn = '2016-09-22_08-44-39.csv'
	var stream = fs.createReadStream(__dirname + '/data/' + fn)
	gloCounter = 0;
	var csvStream = csv
		.fromStream(stream, {headers : true /*fields.slice(0, -2)*/})
	    .on("data", function(data) {			
			saveAndEmit(data, 'pastposition')
			console.log('adding '+gloCounter+','+data.accelerometerAccelerationX)
			socket.emit('dataSet', [gloCounter++, data.accelerometerAccelerationX] )
	    })
	    .on("end", function(){
	         console.log("done reading file "+fn)
	    })	

	// stream.pipe(csvStream)

	socket.on('requestposition', function(msg) {
		socket.emit('sendposition', { "x":msg.lat, "y":msg.lng } )
	})

})

MongoClient.connect('mongodb://New-iMac.local:27017/iosdata', (err, database) => {
    if (err) return console.log(err)
    db = database
    server.listen(config.web.port, () => {
        console.log('listening on '+config.web.port)
    })
})

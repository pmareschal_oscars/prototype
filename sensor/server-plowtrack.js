const express = require('express')
const bodyParser= require('body-parser')
const app = express()
const config = require('./config')

var sensor = require('./lib/sensor')

var server = require('http').Server(app)
var io = require('socket.io')(server)

var cjson = require('cjson');

app.use('/bower', express.static(__dirname + '/bower_components'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/playback.html')
})

io.on('connection', function(socket) {

	var fn = process.argv[2]
	var plow = cjson.load(fn)

	plow.features.sort(function(a, b) {
		return parseInt(a.properties.DateTime_) - parseInt(b.properties.DateTime_)
	})

	var multiPoints = [];
	var multiTimes = [];
	var multiPlot1 = [];
	
	for(var i = 0; i < plow.features.length; i++) {
		var p = plow.features[i]
		var val = p.properties.DateTime_
		multiPoints.push(p.geometry.coordinates)
		var d = new Date(val.substr(0,4), val.substr(4,2), val.substr(6, 2), val.substr(8, 2), val.substr(10, 2), val.substr(12, 2),0 )
		multiTimes.push(d.getTime())
		multiPlot1.push(p.properties.speed.replace(" mph",""))
	}

	var fm = {
		type: "Feature",
		geometry: {
	    	type: "MultiPoint",
	    	coordinates: multiPoints
		},
		properties: {
			time: multiTimes,
			data1: multiPlot1
		}
	}
	socket.emit('playbackdata', fm)
	console.log("Read file "+fn+", "+multiPoints.length+" records loaded.")

})

server.listen(config.web.port, () => {
    console.log('listening on '+config.web.port)
})

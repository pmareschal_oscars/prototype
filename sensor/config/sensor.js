var config = {};

config.redis = {};
config.mongodb = {};
config.web = {};

config.fr24feed = {
    host: 'imac.local',
	port: 8000,
    path: '/data.json'
};

config.use_db = false;

config.mongodb.collection = 'sensordata';
config.mongodb.url = 'mongodb://New-iMac.local:27017/sensordata';

config.redis.uri = process.env.DUOSTACK_DB_REDIS;
config.redis.host = 'hostname';
config.redis.port = 6379;
config.web.port = process.env.WEB_PORT || 3000;

module.exports = config;
// from http://stackoverflow.com/questions/5869216/how-to-store-node-js-deployment-settings-configuration-files
// use with
// var config = require('./config');
const express = require('express')
const bodyParser= require('body-parser')
const app = express()
const config = require('./config')

var sensor = require('./lib/sensor')

var server = require('http').Server(app)
var io = require('socket.io')(server)

var fs = require('fs')
var csv = require('fast-csv')

app.use('/bower', express.static(__dirname + '/bower_components'))

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/playback.html')
})

io.on('connection', function(socket) {

	var fn = process.argv[2]
	var stream = fs.createReadStream(__dirname + '/' + fn)
	var multiPoints = [];
	var multiTimes = [];
	var multiPlot1 = [];

	var csvStream = csv
		.fromStream(stream, {headers : true /*fields.slice(0, -2)*/})
	    .on("data", function(data) {
			var p = sensor.makeGeoJsonSensorData(data)
			if(p) {
				multiPoints.push(p.geometry.coordinates)
				multiTimes.push(Math.round(1000 * p.properties.locationTimestamp_since1970, 0))
				multiPlot1.push(Math.round(p.properties.locationMagneticHeading, 0))
			}
	    })
	    .on("end", function(){
			var fm = {
				type: "Feature",
				geometry: {
			    	type: "MultiPoint",
			    	coordinates: multiPoints
				},
				properties: {
					time: multiTimes,
					data1: multiPlot1
				}
			}
			socket.emit('playbackdata', fm)
			console.log("done reading file "+fn)
	    })	

})

server.listen(config.web.port, () => {
    console.log('listening on '+config.web.port)
})

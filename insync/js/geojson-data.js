var geoJSONdata = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
	  "properties": {
		"temp": [11,12,13,12,12,13,14,12,13,12,14,13,12]
      },
      "geometry": {
        "type": "LineString",
        "coordinates": [
          [
            -0.19148826599121094,
            51.50997111626242
          ],
          [
            -0.18762588500976562,
            51.51018479243817
          ],
          [
            -0.182647705078125,
            51.51050530482274
          ],
          [
            -0.17586708068847653,
            51.51168049760791
          ],
          [
            -0.16144752502441406,
            51.51312273822952
          ],
          [
            -0.16101837158203125,
            51.512428331778096
          ],
          [
            -0.15878677368164062,
            51.512588580360244
          ],
          [
            -0.1574993133544922,
            51.510558723334285
          ],
          [
            -0.15458106994628906,
            51.5081548283056
          ],
          [
            -0.15183448791503906,
            51.506017926242464
          ],
          [
            -0.15234947204589844,
            51.504148054725356
          ],
          [
            -0.15123367309570312,
            51.50409462869737
          ],
          [
            -0.15097618103027344,
            51.503346657728635
          ]
        ]
      },
    }
  ]
};

var geoJSONdata2 = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {id: 23, temp: 12},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.19002914428710938,
          51.510077954475555
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 24, temp: 10},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.18316268920898438,
          51.510558723334285
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 25, temp: 9},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.17586708068847653,
          51.51152024583139
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 26, temp: 10},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.16994476318359375,
          51.51237491545877
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 27, temp: 10},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.16453742980957028,
          51.51296249152639
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 28, temp: 11},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.16127586364746094,
          51.51312273822952
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 29, temp: 11},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.1609325408935547,
          51.51237491545877
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 30, temp: 12},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.15852928161621094,
          51.51253516422883
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 31, temp: 13},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.15672683715820312,
          51.5096506001197
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 32, temp: 10},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.15209197998046875,
          51.50623162095866
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 33, temp: 9},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.1521778106689453,
          51.50404120260676
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {id: 34, temp: 10},
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.15320777893066403,
          51.503186376638006
        ]
      }
    }
  ]
};